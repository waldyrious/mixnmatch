#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 3 ) ;

$mp = $lc->getMainProp() ;
$si = $lc->getSubjectItem() ;

// Caching types, to get the "differentiated person" ID
$types = [] ;
$sql = "SELECT * FROM gnd_constants" ;
if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) $types[$o->name] = $o->id ;

// Getting entries without Wikidata but with day-precision birth and death dates
$sql = "
SELECT * FROM gnd
WHERE q IS NULL
AND length(born)=10
AND length(died)=10
AND entry_type={$types['http://d-nb.info/standards/elementset/gnd#DifferentiatedPerson']}
" ;
/*
AND viaf!=''
AND place_of_birth!=''
AND place_of_death!=''
AND occupation!=''
*/

if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) {
	$sparql = "SELECT ?q ?gnd ?viaf {
  ?q wdt:P31 wd:Q5 .
  ?q wdt:P569 '{$o->born}T00:00:00Z'^^xsd:dateTime .
  ?q wdt:P570 '{$o->died}T00:00:00Z'^^xsd:dateTime .
  OPTIONAL { ?q wdt:P227 ?gnd }
  OPTIONAL { ?q wdt:P214 ?viaf }
  }" ;

	$j = getSPARQL ( $sparql ) ;
	$bindings = $j->results->bindings ;
	if ( count($bindings) == 0 ) continue ;
	
	$name = $o->name ;
	if ( preg_match ( '/^(.+?), ([^,]+)$/' , $name , $m ) ) $name = $m[2] . ' ' . $m[1] ;

	foreach ( $bindings AS $b ) {

		// Get Q from SPARQL
		if ( !preg_match ( '/(Q\d+)$/' , $b->q->value , $m ) ) continue ;
		$q = $m[1] ;

		// Check for GND mismatch
		if ( isset($b->gnd) and $o->ext_id!='' and $b->gnd->value != $o->ext_id ) {
			$lc->addReport ( $o->id , $q , $mp , "Item already has different GND: ".$b->gnd->value , 'MISMATCH' ) ;
			continue ;
		}

		// Check for VIAF mismatch
		if ( isset($b->viaf) and $o->viaf!='' and $b->viaf->value != $o->viaf ) {
			$lc->addReport ( $o->id , $q , $mp , "Item already has different VIAF: ".$b->viaf->value , 'MISMATCH' ) ;
			continue ;
		}

		// Check via API
		$lc->wil->loadItem ( $q ) ;
		$i = $lc->wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ; // No such item on Wikidata
		
		// Check name match
		$possible_names = [] ;
		foreach ( $i->j->labels AS $v ) $possible_names[$v->value] = $v->value ;
		foreach ( $i->j->aliases AS $lang => $al ) {
			foreach ( $al AS $v ) $possible_names[$v->value] = $v->value ;
		}

		$match = false ;
		foreach ( $possible_names AS $bn ) {
			if ( $bn == '' ) continue ; // Paranoia
			if ( $bn == $name ) $match = true ; // Perfect name match
			else if ( preg_match ( '/^(\S+).+(\S+)$/' , $name , $m1 ) and preg_match ( '/^(\S+).+(\S+)$/' , $bn , $m2 ) ) { // First and last name only
				if ( $m1[1]==$m2[1] and $m1[2]==$m2[2] ) $match = true ;
			}
			if ( $match ) break ; // SHortcut
		}
		if ( !$match ) continue ;
		

		// GNDs
		$mpc = $i->getStrings ( $mp ) ;
		if ( count($mpc) > 0 and !in_array ( $o->ext_id , $mpc ) ) {
			$lc->addReport ( $o->id , $q , $mp , "Item already has different GND: ".implode(', ',$mpc) , 'MISMATCH' ) ;
			continue ;
		}
		if ( count($mpc) > 0 ) continue ; // Already has this GND

		// VIAFs
		if ( $o->viaf != '' ) {
			$mpc = $i->getStrings ( 'P214' ) ;
			if ( count($mpc) > 0 and !in_array ( $o->viaf , $mpc ) ) {
				$lc->addReport ( $o->id , $q , $mp , "Item already has different VIAF: ".implode(', ',$mpc) , 'MISMATCH' ) ;
				continue ;
			}
		}
		
		if ( $lc->hasPropertyEverBeenRemovedFromItem ( $q , $mp ) ) {
			$lc->addReport ( $o->id , $q , $mp , "A GND was removed from this item before" , 'REMOVED_BEFORE' ) ;
			continue ;
		}
		
		$source = "\tS813\t" . $lc->getCurrentDate() ;
		if ( $lc->getCat()->subject*1 > 0 ) $source .= "\tS248\t" . $lc->getSubjectItem() ;
		
		
		$commands = [] ;
		$commands[] = "$q\t$mp\t\"{$o->ext_id}\"$source" ;

		if ( $o->viaf!='' and !$i->hasClaims('P214') and !$lc->hasPropertyEverBeenRemovedFromItem ( $q , 'P214' ) ) {
			$commands[] = "$q\tP214\t\"{$o->viaf}\"$source" ;
		}

		$lc->runQS ( implode("\n",$commands) ) ;
//		print_r ( $commands ) ; print "\n" ;
	}
}

?>