#!/usr/bin/php
<?PHP

$catalogs = [] ; //array ( 780 , 755 , 648 , 169 , 589 , 560 , 558 , 540 , 505 , 500 , 502 , 392 , 314 , 193 , 231 , 255 ) ;

$use_desc = [ 827 ] ;

$ranks = array (
	'variety' => 'Q767728' ,
	'subspecies' => 'Q68947' ,
	'species' => 'Q7432' ,
	'superfamily' => 'Q2136103' ,
	'subfamily' => 'Q2455704' ,
	'class' => 'Q37517' ,
	'suborder' => 'Q5867959' ,
	'genus' => 'Q34740' ,
	'family' => 'Q35409' ,
	'order' => 'Q36602' ,
	'Q16521' => '' 
) ;

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$db = openMixNMatchDB() ;

if ( isset($argv[1]) ) { // Use single command line parameter catalog
	$catalogs = array ( $argv[1] ) ;
} else { // Use all catalogs with an entry type "taxon"
	$sql = "select distinct catalog from entry where `type`='Q16521'" ;
	$result = getSQL ( $db , $sql , 2 ) ;
	while($o = $result->fetch_object()) $catalogs[] = $o->catalog ;
}


$found = array() ;
$sql = "SELECT * FROM entry WHERE catalog IN (" . implode(',',$catalogs) . ") AND (q IS NULL OR user=0)" ;
$sql .= " AND `type` IN ('" . implode("','",array_values($ranks)) . "Q16521')" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()){
	if ( in_array($o->catalog,$use_desc) ) $taxon = $o->ext_desc ;
	else $taxon = $o->ext_name ;

	$type = $o->type ;
	if ( !isset($ranks[$type]) ) {
		foreach ( $ranks AS $k => $v ) {
			if ( $v == $type ) $type = $k ;
		}
	}

	if ( $o->catalog == 169 ) {
		if ( preg_match ( '/\[([a-z ]+)[,\]]/i' , $o->ext_desc , $m ) ) { $taxon = $m[1] ; # First in []
//		if ( preg_match ( '/\[.+, ([a-z ]+)]/i' , $o->ext_desc , $m ) ) { $taxon = $m[1] ; # Last in []
		} else {
			$type = '' ;
			foreach ( $ranks AS $k => $v ) {
				if ( preg_match ( '/ '.$k.'$/i' , $o->ext_desc ) ) $type = $k ;
				if ( preg_match ( '/^'.$k.' of /i' , $o->ext_desc ) ) $type = $k ;
			}
			if ( $type == '' ) continue;
		}
//		print "$taxon\t$type\n" ; continue ;
	}

	$taxon = preg_replace ( '/ ssp\. /' , ' subsp. ' , $taxon ) ;
//	if ( !isset($ranks[$type]) and $type != 'Q16521' ) continue ; // Q16521 = taxon
	$rank = $ranks[$type] ;
	$t2 = $db->real_escape_string($taxon) ;
	$sparql = "SELECT ?q { ?q wdt:P31/wdt:P279* wd:Q16521 . { { VALUES ?prop { wdt:P225 } . ?q ?prop '$t2' } UNION { ?q skos:altLabel '$t2'@en } } " ; # Q23038290
	if ( isset($rank) AND $rank != '' ) $sparql .= ". ?q wdt:P105 wd:$rank" ;
	$sparql .= " }" ;

	$items = getSPARQLitems ( $sparql ) ;
	if ( count($items) != 1 ) continue ;
	$q = $items[0] ;
	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET timestamp='$ts',user=4,q=$q WHERE id={$o->id} AND (q IS NULL OR user=0)" ;
	if ( !$db->ping() ) $db = openMixNMatchDB() ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	if ( !isset($found[$o->catalog]) ) $found[$o->catalog] = 0 ;
	$found[$o->catalog]++ ;
}

// Update stats
foreach ( $found AS $catalog => $cnt ) {
	file_get_contents ( 'https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog='.$catalog ) ;
}

?>
