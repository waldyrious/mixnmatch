<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/mix-n-match/opendb.inc' ) ;

class MixNMatch {
	public $dbm ;
	
	function __construct() {
		$this->dbm = $this->openMixNMatchDB() ;
	}

	private function openMixNMatchDB () {
		$db = openToolDB ( 'mixnmatch_p' ) ;
		if ( $db === false ) die ( "Cannot access DB: " . $o['msg'] ) ;
		$db->set_charset("utf8") ;
		return $db ;
	}

	public function escape ( $s ) {
		return $this->dbm->real_escape_string ( $s ) ;
	}


} ;

?>