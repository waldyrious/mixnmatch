#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$filename = '/data/project/mix-n-match/manual_lists/catalog_69.tab' ;
$num_header_rows = 1 ;
$catalog = 69 ;
$add_new = true ;
$update_existing_description = true ;

$min_cols = 7 ;
$col_id = 0 ;
$col_name = 1 ;
$col_url = -1 ;
$col_type = -1 ;
$col_desc = -1 ;
$col_born = 4 ;
$col_died = 6 ;
$born_pattern = '/^(\d+)$/' ;
$died_pattern = '/^(\d+)$/' ;

//________________________________________________________________________________________________________________

$mnm = new MixNMatch ;

$rows = explode ( "\n" , file_get_contents ( $filename ) ) ;
foreach ( $rows AS $row ) {
	if ( $num_header_rows-- > 0 ) continue ;
	$parts = explode ( "\t" , $row ) ;
	if ( count($parts) < 3 ) continue ;
	while ( count($parts) < $min_cols ) $parts[] = '' ;
	foreach ( $parts AS $k => $v ) $parts[$k] = trim($v) ;
	
	$id = $parts[$col_id] ; # Required
	$name = $parts[$col_name] ; # Required
	$url = ( $col_url == -1 ) ? '' : $parts[$col_url] ;
	$type = ( $col_type == -1 ) ? '' : $parts[$col_type] ;
	$desc = [] ;
	if ( $col_desc == -1 ) {
		foreach ( $parts AS $col => $text ) {
			if ( $col==$col_id or $col==$col_name or $col==$col_url or $col==$col_type ) continue ;
			if ( trim($text) == '' ) continue ;
			$desc[] = trim($text) ;
		}
	} else $desc[] = $parts[$col_desc] ;
	$desc = implode ( "; " , $desc ) ;

	$born = '' ;
	if ( $col_born!=-1 and preg_match ( $born_pattern , $parts[$col_born] , $m ) ) $born = $m[1] ;
	$died = '' ;
	if ( $col_died!=-1 and preg_match ( $died_pattern , $parts[$col_died] , $m ) ) $died = $m[1] ;

	$sql = "SELECT * FROM entry WHERE catalog=" . $mnm->escape($catalog) . " AND ext_id='" . $mnm->escape($id) . "'" ;
	$result = getSQL ( $mnm->dbm , $sql ) ;
	$entry = (object) [] ;
	while($o = $result->fetch_object()) $entry = $o ;

	if ( isset($entry->id) ) { // Entry exists

		if ( $update_existing_description AND $entry->ext_desc == '' and $desc != '' and $entry->ext_desc != $desc ) {
			$sql = "UPDATE entry SET ext_desc='" . $mnm->escape($desc) . "' WHERE id={$entry->id}" ;
			getSQL ( $mnm->dbm , $sql ) ;
		}

	} else { // New entry

		if ( $add_new ) {
			$sql = "INSERT IGNORE INTO entry (catalog,ext_name,ext_desc,ext_url,`type`,random) VALUES (" ;
			$sql .= $mnm->escape($catalog) . ',' ;
			$sql .= '"' . $mnm->escape($name) . '",' ;
			$sql .= '"' . $mnm->escape($desc) . '",' ;
			$sql .= '"' . $mnm->escape($url) . '",' ;
			$sql .= '"' . $mnm->escape($type) . '",' ;
			$sql .= "rand())" ;
			getSQL ( $mnm->dbm , $sql ) ;
			$entry->id = $mnm->dbm->insert_id ;
		}
		
	}

	if ( $born.$died != '' and isset($entry->id) ) {
		$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died,in_wikidata) VALUES ({$entry->id},'".$mnm->escape($born)."','".$mnm->escape($died)."',0)" ;
		getSQL ( $mnm->dbm , $sql ) ;
	}
}

?>