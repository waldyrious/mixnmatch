#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/mix-n-match/opendb.inc' ) ;

$db = openMixNMatchDB() ;

if ( 1 ) {
	$sql = 'SELECT * FROM entry WHERE catalog=169 AND q is null and ext_desc like "film by %" AND NOT EXISTS (SELECT * FROM `multi_match` WHERE entry.id=entry_id) AND ext_desc NOT lIKE "%;%"' ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) {
		$year = '' ;
		$director = '' ;
		if ( preg_match ( '/film by (\S+) \[(\d+)\]$/' , $o->ext_desc , $m ) ) list ( , $director , $year ) = $m ;
		else if ( preg_match ( '/film by (\S+)$/' , $o->ext_desc , $m ) ) $director = $m[1] ;
		else continue ;

		$sparql = 'SELECT DISTINCT ?q_work {
		  ?q_work wdt:P31/wdt:P279* wd:Q11424 .
		  ?q_work rdfs:label ?work_label .
		  FILTER ( STR(?work_label)="' . $o->ext_name . '" && LANG(?work_label)="en")
		  ?q_work wdt:P57 ?q_author .
		  ?q_author wdt:P31 wd:Q5 .
		  ?q_author rdfs:label ?author_label .
		  FILTER ( CONTAINS(STR(?author_label),"' . $director . '") && LANG(?author_label)="en")' ;
		if ( $year != '' ) $sparql .= " ?q_work wdt:P577 ?date . FILTER ( YEAR(?date)=$year ) " ;
		$sparql .= '}' ;
		$items = getSPARQLitems ( $sparql , 'q_work' ) ;

		if ( count($items) != 1 ) continue ;
	
		$q = $items[0]*1 ;
	
		$sql = "UPDATE entry SET q=$q,user=4,timestamp='20171018155700' WHERE id={$o->id} AND (q IS NULL or user=0)" ;
		if ( 1 ) {
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		} else {
			print_r ( $o ) ;
			print "$q\n$sql\n" ;
			exit(0);
		}

	}
}

if ( 0 ) { // Perfect name/desc
	$sql = "SELECT * FROM entry WHERE catalog=169 AND (user=0 OR q is null) AND ext_desc!=''" ;
#$sql .= " AND id=9501563" ; # TESTING
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	while($o = $result->fetch_object()) {
		if ( strlen($o->ext_desc) < 3 ) continue ; // Paranoia
		$items_with_label = [] ;
		$sql = "SELECT * FROM wb_terms WHERE term_language='en' AND term_type='label' AND term_text='" . $dbwd->real_escape_string($o->ext_name) . "'" ;
		if(!$result2 = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
		while($p = $result2->fetch_object()) $items_with_label[$p->term_full_entity_id] = 1 ;
		$items = [] ;
		$sql = "SELECT * FROM wb_terms WHERE term_language='en' AND term_type='description' AND term_text='" . $dbwd->real_escape_string($o->ext_desc) . "'" ;
		if(!$result2 = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
		while($p = $result2->fetch_object()) {
			if ( isset($items_with_label[$p->term_full_entity_id]) ) $items[] = $p->term_full_entity_id ;
		}
		if ( count($items) != 1 ) continue ;
		$q = preg_replace ( '/\D/' , '' , $items[0] ) ;
		$sql = "UPDATE entry SET q=$q,user=4,timestamp='20171017090900' WHERE id={$o->id} AND (q IS NULL or user=0)" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	}
}




if ( 0 ) { // Work
	$type2q = [
		'book' => [ 'Q571' , 'P50' ] ,
		'work' => [ 'Q571' , 'P50' ] ,
		'novel' => [ 'Q571' , 'P50' ] ,
		'novella' => [ 'Q571' , 'P50' ] ,
		'graphic novel' => [ 'Q725377' , 'P50' ] ,
		'sculpture' => [ 'Q860861' , 'P170' ] ,
		'poem' => [ 'Q5185279' , 'P50' ] ,
		'play' => [ 'Q25379' , 'P50' ] ,
		'album' => [ 'Q482994' , 'P175' ] ,
		'painting' => [ 'Q3305213' , 'P170' ] ,
#		'film' => [ 'Q11424' , 'P57' ] , # Special code above
		'opera' => [ 'Q1344' , 'P86' ] ,
		'symphony' => [ 'Q9734' , 'P86' ] ,
	] ;

	$sql = 'SELECT * FROM entry WHERE catalog=169 AND (user=0 OR q is null) AND ext_desc LIKE "% by %" ORDER BY rand()' ;
	#$sql = 'SELECT * FROM entry WHERE id=9661837' ; // TESTING
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) {
		if ( !preg_match ( '/^(\S+) by (.+)$/' , $o->ext_desc , $m ) ) continue ;
		if ( !isset($type2q[$m[1]]) ) continue ;
		$qt = $type2q[$m[1]][0] ;
		$p_author = $type2q[$m[1]][1] ;
		$author = trim ( preg_replace ( '/\[.+\]/' , '' , $m[2] ) ) ;
	
	#print "Trying '{$o->ext_name}' : {$o->ext_desc}\n" ;

		$sparql = 'SELECT DISTINCT ?q_work {
	  ?q_work wdt:P31/wdt:P279* wd:' . $qt . ' .
	  ?q_work rdfs:label ?work_label .
	  FILTER ( STR(?work_label)="' . $o->ext_name . '" && LANG(?work_label)="en")
	  ?q_work wdt:' . $p_author . ' ?q_author .
	  ?q_author wdt:P31 wd:Q5 .
	  ?q_author rdfs:label ?author_label .
	  FILTER ( CONTAINS(STR(?author_label),"' . $author . '") && LANG(?author_label)="en")
	}' ;
		$items = getSPARQLitems ( $sparql , 'q_work' ) ;
	#print "$sparql\n" ;
	#print_r ( $items ) ;
		if ( count($items) != 1 ) continue ;
	
		$q = $items[0]*1 ;
	
		$sql = "UPDATE entry SET q=$q,user=4,timestamp='20171016135700' WHERE id={$o->id} AND (q IS NULL or user=0)" ;
		if ( 1 ) {
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		} else {
			print_r ( $o ) ;
			print "$q\n" ;
			exit(0);
		}
	}
}

file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=169" ) ; // Update stats

?>