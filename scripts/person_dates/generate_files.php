#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( '../../public_html/php/common.php' ) ;
require_once ( '../../opendb.inc' ) ;

$pwd = '/data/project/mix-n-match/scripts/person_dates/out' ;

function fix_string ( $s ) {
	$s = preg_replace ( '/[\n\t]/' , ' ' , $s ) ;
	return $s ;
}

exec ( "rm -rf $pwd/*" ) ;

$ages = array() ;
$files = array () ;

$db = openMixNMatchDB() ;
$sql = "SELECT * FROM entry,person_dates WHERE entry.id=entry_id" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) {
	$key = strlen($o->born) . '_' . strlen($o->died) ;
	if ( !isset($files[$key]) ) {
		$fn = "$pwd/out.$key.dates" ;
		$files[$key] = array ( $fn , fopen ( $fn , 'w' ) ) ;
	}
	$out = array () ;
	$out[] = $o->born ;
	$out[] = $o->died ;
	$out[] = $o->id ;
	$out[] = fix_string ( $o->ext_id ) ;
	$out[] = fix_string ( $o->ext_name ) ;
	$out[] = fix_string ( $o->ext_desc ) ;
	$out[] = ( $o->user <= 0 or $o->q == null or !isset($o->q) ) ? '' : $o->q ;
	$out[] = $o->catalog ;
	$out = implode ( "\t" , $out ) . "\n" ;
	fwrite ( $files[$key][1] , $out ) ;
	
	if ( preg_match ( '/^(\d{3,4})/' , $o->born , $b ) and preg_match ( '/^(\d{3,4})/' , $o->died , $d ) ) {
		$age = $d[1]*1 - $b[1]*1 ;
		if ( $age < 0 or $age > 120 ) print $o->catalog . "\n" ;
		$ages[$o->catalog][$age]++ ;
	}
}

foreach ( $files AS $file ) fclose ( $file[1] ) ;

foreach ( $files AS $file ) {
	$fn = $file[0] ;
	exec ( "sort '$fn' > '$fn.tmp'" ) ;
	unlink ( $fn ) ;
	rename ( "{$fn}.tmp" , $fn ) ;
}

$fh = fopen ( "$pwd/ages" , 'w' ) ;
fwrite ( $fh , "catalog\tage\tcount\tpercent\n" ) ;
foreach ( $ages AS $catalog => $ages ) {
	ksort ( $ages , SORT_NUMERIC ) ;
	$total = 0 ;
	foreach ( $ages AS $age => $cnt ) $total += $cnt ;
	foreach ( $ages AS $age => $cnt ) {
		$out = "$catalog\t$age\t$cnt\t" . ($cnt*100/$total) . "\n" ;
		fwrite ( $fh , $out ) ;
	}
}
fclose ( $fh ) ;

?>