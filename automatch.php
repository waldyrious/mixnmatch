#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog = $argv[1] ;
$main_language_only = false ;

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

function getSearch ( $query ) {
	return json_decode ( file_get_contents ( "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ) ) ;
}

$db = openMixNMatchDB() ;

print "Processing catalog $catalog\n" ;

$main_language = 'en' ; // Fallback

$found = false ;
$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( $main_language_only and $o->search_wp != '' ) $main_language = $o->search_wp ;
	if ( $o->active != 1 ) exit ( 0 ) ;
	$found = true ;
}
if ( !$found ) exit ( 0 ) ; // Catalog does not exist


$types = array() ;
$name2ids = array() ;
$sql = "SELECT id,lower(ext_name) AS name,`type` FROM entry WHERE catalog=$catalog and q IS NULL" ;
$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry=entry.id AND log.action='remove_q')" ;
#$sql .= " AND `type`='Q17537576'" ; # TESTING
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$n = trim($o->name) ;
	$n = preg_replace ( '/^([Ss]ir|[Ll]ady|[Dd]ame) /' , '' , $n ) ;
	$name2ids[$n][] = $o->id ;
	
	if ( preg_match ( '/-/' , $n ) ) $name2ids[str_replace('-',' ',$n)][] = $o->id ;
	if ( preg_match ( '/\(/' , $n ) ) $name2ids[preg_replace('/\s+\(.*?\)/','',$n)][] = $o->id ; // ()

	if ( preg_match ( '/\s*[A-Z]\.{0,1}\s+/' , $n ) ) {
		$name2ids[trim(preg_replace('/\s*[A-Z]\.{0,1}\s+/',' ',$n))][] = $o->id ; // Single letter
		print "$name\t" . trim(preg_replace('/\s*[A-Z]\.{0,1}\s+/',' ',$n)) . "\n" ; exit(0);
	}

	
	if ( $o->type != 'person' and preg_match ( '/^(.+)s$/' , $n , $m ) ) { // Plural => singular, for non-people
		$n2 = $m[1] ;
		if ( preg_match ( '/^(.+)ies$/' , $n , $m ) ) $n2 = $m[1].'y' ;
		$name2ids[$n2][] = $o->id ;
		if ( preg_match ( '/-/' , $n2 ) ) $name2ids[str_replace('-',' ',$n2)][] = $o->id ;
	}
	
	if ( $o->type != '' ) $types[$o->type]++ ;
}


$sql = "SELECT limiter FROM catalog WHERE id=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( trim($o->limiter) == '' ) break ;
	$j = json_decode ( $o->limiter ) ;
	foreach ( $j AS $type ) $types[$type]++ ;
}
if ( isset ( $types['person'] ) ) {
	$types['Q5'] += $types['person'] ;
	unset ( $types['person'] ) ;
}

$linkto = array() ;

if ( $argv[2] != 'notype' ) {
	$types = array_keys($types) ;
	if ( count($types) > 0 ) {
	#	print_r ( $types ) ;
		$sparql = "SELECT ?q { VALUES ?types { wd:" . implode ( ' wd:' , $types ) . " } ?q wdt:P279* ?types }" ;
	#	print "$sparql\n" ;
		$items = getSPARQLitems ( $sparql ) ;
		foreach ( $items AS $i ) $linkto[] = "Q$i" ;
	#	print_r ( $linkto ) ;
	}
}

$dbwd = openDB ( 'wikidata' , true ) ;
$names = array() ;
foreach ( $name2ids AS $k => $v ) {
	$names[] = $dbwd->real_escape_string ( $k ) ;
}
if ( count($names) == 0 ) exit(0) ;


$multimatch = array() ;
while ( count($names) > 0 ) {
	$candidates = array() ;
	$names2 = array() ;
	while ( count($names) > 0 and count($names2) < 100 ) $names2[] = array_pop ( $names ) ;
	$dbwd = openDB ( 'wikidata' , true ) ;
	$sql = "SELECT term_search_key AS name,term_entity_id AS q FROM wb_terms AS terms WHERE " ; // ,count(DISTINCT term_entity_id) AS cnt
	$sql .= " term_type IN ('label','alias') and term_search_key IN ('" . implode("','",$names2) . "') and term_entity_type='item'" ;
	if ( $main_language_only ) $sql .= " AND term_language='" . $dbwd->real_escape_string ( $main_language ) . "' " ;
/*	
	if ( count($linkto) > 0 ) {
		$sql .= " AND term_entity_id IN (select epp_entity_id from wb_entity_per_page,pagelinks WHERE pl_from=epp_page_id AND epp_entity_type='item' AND pl_namespace=0 AND pl_title IN ( '".implode("','",$linkto)."' ))" ;
	}
*/	
	$sql .= " group by name,q" ;
#	$sql .= " HAVING cnt=1" ;
#	print "$sql\n" ; exit(0);

	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	
	
	$name2q = array() ;
	$qlist = array() ;
	while($o = $result->fetch_object()){
		$name2q[$o->name][$o->q] = $o->q ;
		$qlist[$o->q] = $o->q ;
	}
	
	if ( count($qlist) == 0 ) continue ; // Nothing to do
	
	// Filter disambiguation candidates
	$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_namespace=0 AND page_title IN ('Q" . implode("','Q",$qlist) . "') AND pl_from=page_id AND pl_title IN ('Q4167410','Q11266439','Q4167836','Q13406463')" ;
	$dbwd = openDB ( 'wikidata' , true ) ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	while($o = $result->fetch_object()){
		$q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
		unset ( $qlist[$q] ) ;
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($qs[$q]) ) continue ;
			unset ( $name2q[$name][$q] ) ;
			if ( count($name2q[$name]) == 0 ) unset ( $name2q[$name] ) ;
		}
	}
	
	
	if ( count($qlist) == 0 ) continue ; // Nothing to do

	if ( count($linkto) > 0 ) { // Filter by type
		$goodq = array() ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_namespace=0 AND page_title IN ('Q" . implode("','Q",$qlist) . "') AND pl_from=page_id AND pl_title IN ( '".implode("','",$linkto)."' )" ;
		$dbwd = openDB ( 'wikidata' , true ) ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
		while($o = $result->fetch_object()){
			$q = preg_replace ( '/^Q/' , '' , $o->page_title ) ;
			$goodq[$q] = $q ;
		}
		
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			$maybe = [] ;
			foreach ( $qs AS $q ) {
				if ( isset($goodq[$q]) ) $maybe[$q] = $q ;
			}
			if ( count($maybe) != 1 ) {
				if ( count($maybe) > 1 ) { foreach ( $name2ids[$name] AS $id ) { foreach($maybe AS $m) $multimatch[$id][$m] = $m ; } }
				continue ;
			}
			foreach ( $maybe AS $q ) {
				foreach ( $name2ids[$name] AS $id ) $candidates[''.$id] = $q ;
			}
		}
	} else {
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			if ( count($qs) != 1 ) {
				foreach ( $name2ids[$name] AS $id ) { foreach($qs AS $m) $multimatch[$id][$m] = $m ; }
				continue ;
			}
			foreach ( $qs AS $q ) {
				foreach ( $name2ids[$name] AS $id ) $candidates[''.$id] = $q ;
			}
		}
	}

	$db2 = openMixNMatchDB() ;
	$ts = date ( 'YmdHis' ) ;
	foreach ( $candidates AS $entry => $q ) {
		$sql = "UPDATE entry SET q=$q,user=0,timestamp='$ts' WHERE id=$entry AND q IS NULL" ;
		if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']');
	}

	
}

# Multimatch
$db2 = openMixNMatchDB() ;
$sql = "DELETE FROM multi_match WHERE catalog=$catalog" ;
if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']');
foreach ( $multimatch AS $entry => $list ) {
	if ( count($list) >= 10 ) continue ; # Too many to be useful
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ($entry,$catalog,'" . implode(',',$list) . "'," . count($list) . ")" ;
	if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']');
}




file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=$catalog" ) ; // Update stats

?>
