#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;


function DMStoDEC($deg,$min,$sec) {
    return $deg+((($min*60)+($sec))/3600);
}    

function pad ( $s , $digits ) {
	while ( strlen($s) < $digits ) $s = "0$s" ;
	return $s ;
}

function dp ( $d ) {
	if ( preg_match ( '/^\d+$/' , $d ) ) return $d ;
	$d = date_parse ( $d ) ;
	while ( strlen($d['year']) < 4 ) $d['year'] = '0' . $d['year'] ;
	while ( strlen($d['month']) < 2 ) $d['month'] = '0' . $d['month'] ;
	while ( strlen($d['day']) < 2 ) $d['day'] = '0' . $d['day'] ;
	return $d['year'] . '-' . $d['month'] . '-' . $d['day'] ;
}



if ( isset($argv[1]) ) $catalog = $argv[1] ;
else die ( "Catalog required" ) ;

$testing = 0 ;

$db = openMixNMatchDB() ;
$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
if ( $catalog == 169 ) $sql .= " AND `type` IN ('person','Q5') AND (q IS NULL or user=0)" ;
else if ( $catalog != 917 and $catalog != 389 and $catalog != 91 and $catalog != 344 and $catalog != 547 and $catalog != 323 ) $sql .= " AND ext_desc='' AND (q IS NULL or user=0)" ;

#if ( $catalog == 958 ) $sql .= " AND id=38075251" ; # TESTING

if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	if ( trim($o->ext_url) == '' ) continue ;
if ( $testing ) print "Trying {$o->ext_url}\n" ;
	$html = @file_get_contents ( $o->ext_url ) ;
	if ( $html === false ) continue ;

	$html = preg_replace ( '/\s+/' , ' ' , $html ) ; // Simple spaces
	
	$born = '' ;
	$died = '' ;
	$d = array() ;
	if ( $catalog == 442 ) {
		if ( preg_match ( '/(Né\(e\) .+?)<br/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(Décédé\(e\) .+?)<br/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 52 ) {
		if ( preg_match ( '/<div class="artist-header-birthdate">(.*?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 980 ) {
		if ( preg_match ( '/<h3>Profile<\/h3>(.+?)<h4>Share<\/h4>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 934 ) {
		if ( preg_match ( '/<b>Author: <\/b>(.+?)<\/td>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 917 ) {
		if ( preg_match ( '/<td valign="top" align="left" width="60%">(.+?)<p align="right">/' , $html , $m ) ) {
			$d2 = preg_replace ( '/<.+?>/' , ' ' , $m[1] ) ;
			$d2 = str_replace ( '&nbsp;' , ' ' , $d2 ) ;
			$d2 = html_entity_decode ( $d2 ) ;
			$d2 = trim ( preg_replace ( '/[\s ]+/' , ' ' , $d2 ) ) ;
			if ( preg_match ( '/luminous-lint/' , $d2 ) ) $d2 = 'photographer' ;
			$d[] = $d2 ;
		}
	} else if ( $catalog == 797 ) {
		$d2 = explode ( '<dl class=dl-horizontal>' , $html , 2 ) ;
		if ( count($d2) != 2 ) continue ;
		$d2 = explode ( '<dd itemprop=copyrightHolder>' , $d2[1] , 2 ) ;
		if ( count($d2) != 2 ) continue ;
		$d2 = $d2[0] ;
		$d2 = preg_replace ( '/<dt>.*?<\/dt>/' , '' , $d2 ) ;
		$d2 = preg_replace ( '/<\/dd>/' , '; ' , $d2 ) ;
		$d2 = preg_replace ( '/<.*?>/' , ' ' , $d2 ) ;
		$d2 = preg_replace ( '/\s+;/' , ';' , $d2 ) ;
		$d2 = preg_replace ( '/\s+/' , ' ' , $d2 ) ;
		$d2 = preg_replace ( '/;\s*$/' , '' , $d2 ) ;
		$d2 = trim ( $d2 ) ;
		$d[] = $d2 ;
	} else if ( $catalog == 958 ) {

		if ( preg_match ( '/<\/h2>\((.+?)\)</' , $html , $m ) ) {
			$d2 = $m[1] ;
			$d2 = preg_replace ( '/<.*?>/' , '' , $d2 ) ;
			$d[] = $d2 ;
		}

		if ( preg_match ( '/https{0,1}:\/\/viaf\.org\/viaf\/(\d+)/' , $html , $m ) ) {
			$sparql = "SELECT ?q { ?q wdt:P214 '" . $m[1] . "' }" ;
			$items = getSPARQLitems ( $sparql ) ;
			if ( count($items) == 1 ) {
				$sql = "UPDATE entry SET q={$items[0]},user=4,timestamp='20180130100000' WHERE id={$o->id} AND (q IS null OR user=0)" ;
				getSQL ( $db , $sql , 2 ) ;
			}
		}

	} else if ( $catalog == 913 ) {
		if ( preg_match ( '/<div itemscope itemtype="http:\/\/schema\.org\/Person">(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/' , '' , $m[1] ) ;
	} else if ( $catalog == 61 ) {
		if ( preg_match ( '/<div class="index">(.{1,250})/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 723 ) {
		if ( preg_match ( '/<meta property="og:description" content="(.+?)"\s*\/>/' , $html , $m ) ) $d[] = trim($m[1]) ;
		if ( preg_match ( '/\((\d{4})-(\d{4})\)/' , $d[0] , $m ) ) list ( , $born , $died ) = $m ;
		if ( preg_match ( '/<div>Born <span.*?>(.+?)<\/span><\/div>/' , $html , $m ) ) $born = dp ( $m[1] ) ;
		if ( preg_match ( '/<div>Died <span.*?>(.+?)<\/span><\/div>/' , $html , $m ) ) $died = dp ( $m[1] ) ;
	} else if ( $catalog == 690 ) {
		if ( preg_match ( '/\bborn (\d{1,2} \S+, \d{3,4})/i' , $html , $m ) ) $born = dp($m[1]) ;
		if ( preg_match ( '/\bdied (\d{1,2} \S+, \d{3,4})/i' , $html , $m ) ) $died = dp($m[1]) ;
	} else if ( $catalog == 323 ) {
		if ( preg_match ( '/\(geb. .*?(\d{1,2})-(\d{1,2})-(\d{3,4}) –/' , $html , $m ) ) $born = pad($m[3],4).'-'.pad($m[2],2).'-'.pad($m[1],2) ;
		if ( preg_match ( '/gest. .*?(\d{1,2})-(\d{1,2})-(\d{3,4})\)/' , $html , $m ) ) $died = pad($m[3],4).'-'.pad($m[2],2).'-'.pad($m[1],2) ;
	} else if ( $catalog == 154 ) {
	} else if ( $catalog == 73 ) {
		if ( preg_match ( '/<meta property="og:description" content="(.+?)" \/>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 465 ) {
		if ( preg_match ( '/<\/h2>(.+?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 168 ) {
		if ( preg_match ( '/<span class="tspPrefix">Nationality\/Dates<\/span><span class="tspValue">(.*?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 169 ) {
		if ( trim($o->ext_desc) != '' ) $d[] = trim($o->ext_desc) ;
		if ( preg_match ( '/\((born.+?)\)/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/<strong>Alternative Titles{0,1}:<\/strong>(.+?)<\/div>/' , $html , $m ) ) $d[] = '['.trim($m[1]).']' ;
	} else if ( $catalog == 547 ) {
		if ( preg_match ( '/<META NAME="description" CONTENT=".*?\[(.*?)\].*?">/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 344 ) {
		if ( preg_match ( '/<div class="biog">(.*?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 619 ) {
		if ( preg_match ( '/<h4 class="widgettitle"> Artist Data <\/h4>(.+)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 541 ) {
		if ( preg_match ( '/<div id="contenedorInfoAutor">\s*<table>(.*?)<\/table>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 91 ) {

		if ( preg_match ( '/(<b>Born:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Died:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Occupation:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Sex:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Nationality:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 389 ) {
	
		if ( preg_match ( '/window.location="(.+?)"/' , $html , $m ) ) {
			$html = @file_get_contents ( $m[1] ) ;
			if ( $html === false ) continue ;
			$html = preg_replace ( '/\s+/' , ' ' , $html ) ; // Simple spaces
		}
	
		if ( !preg_match ( '/<h3>(Locatie|Location)\s*\(*(.+?)\)*<\/h3>/' , $html , $m ) ) continue ;
		
		$s = $m[2] ;
		$s = str_replace ( 'N.B.' , 'N' , $s ) ;
		$s = str_replace ( 'S.B.' , 'S' , $s ) ;
		$s = str_replace ( 'O.L.' , 'E' , $s ) ;
		$s = str_replace ( 'W.L.' , 'W' , $s ) ;
		if ( !preg_match ( '/^([NS])\s*(\d+)&deg;(\d+)&#39;(\d+)&quot\s*[;\- ]+\s*([EW])\s*(\d+)&deg;(\d+)&#39;(\d+)&quot;/' , $s , $n ) ) continue ;
		$lat = DMStoDEC ( ($n[1]=='N'?1:-1)*$n[2] , $n[3]*1 , $n[4]*1 ) ;
		$lon = DMStoDEC ( ($n[5]=='E'?1:-1)*$n[6] , $n[7]*1 , $n[8]*1 ) ;
		
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},$lat,$lon)" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		continue ;
	
	} else if ( $catalog == 185 ) {
		if ( preg_match ( '/<h1>.*?\((.+?)\).*?<\/h1>/' , $html , $m ) ) $d[] = preg_replace ( '/ \&ndash;\s*$/' , '-' , $m[1] ) ;
	} else {
		die ( "No code for catalog $catalog" ) ;
	}

	if ( $born != '' ) {
		$sql = "UPDATE person_dates SET born='$born' WHERE entry_id={$o->id}" ;
		getSQL ( $db , $sql ) ;
	}
	if ( $died != '' ) {
		$sql = "UPDATE person_dates SET died='$died' WHERE entry_id={$o->id}" ;
		getSQL ( $db , $sql ) ;
	}

	
	if ( count($d) == 0 ) {
#print "No joy.\n" ;
		continue ;
	}
	$d = implode ( '; ' , $d ) ;
	$d = preg_replace ( '/<.+?>/' , ' ' , $d ) ; // Remove HTML tags
	$d = preg_replace ( '/\s+/' , ' ' , $d ) ; // Simple spaces
	if ( strlen($d) > 250 ) $d = substr ( $d , 0 , 250 ) ;
	$d = $db->real_escape_string ( trim ( $d ) ) ;
	if ( $d == '' or $d == $o->ext_desc ) continue ; // Nope
	$sql = "UPDATE entry SET ext_desc='$d' WHERE id={$o->id}" ;

	if ( $testing ) print "$sql\n" ;
	else if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}



?>