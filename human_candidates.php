#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$bad_catalogs = [ 70 ] ;

$db = openMixNMatchDB() ;
$catalogs = array() ;
$sql = "SELECT id FROM catalog WHERE active=1 AND id NOT IN (" . implode(',',$bad_catalogs) . ")" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) $catalogs[] = $o->id ;
$catalogs = implode ( ',' , $catalogs ) ;

$data = array() ;
#$sql = "SELECT id,ext_name,ext_desc FROM entry WHERE q is null AND type IN ('person','Q5') AND ext_desc REGEXP '[0-9][0-9][0-9][0-9]' AND catalog IN ($catalogs)" ;
$sql = "SELECT id,ext_name,born,died FROM entry,person_dates WHERE q is null AND type IN ('person','Q5') AND entry_id=entry.id AND catalog IN ($catalogs) AND born!='' AND died!=''" ;
$result = getSQL ( $db , $sql , 2 ) ;
#if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()){
	$name_key = trim(preg_replace('/[-]/',' ',strtolower($o->ext_name))) ;
	if ( !preg_match('/^(\d{3,4})/' , $o->born , $by ) ) continue ;
	if ( !preg_match('/^(\d{3,4})/' , $o->died , $dy ) ) continue ;
	$date_key = $by[1].'-'.$dy[1] ;
	$data[$name_key][$date_key][] = $o->id ;
#	if ( !preg_match ( '/(\d{4}).+(\d{4})/' , $o->ext_desc , $m ) ) continue ;
#	$data[trim(preg_replace('/[-]/',' ',strtolower($o->ext_name)))][$m[1].'-'.$m[2]][] = $o->id ;
}

$db = openMixNMatchDB() ;
$sql = "TRUNCATE common_names_human" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
foreach ( $data AS $name => $dates ) {
	if ( preg_match ( '/untitled/' , $name ) ) continue ;
//	if ( count ( explode ( ' ' , $name ) ) > 3 ) continue ;
	foreach ( $dates AS $k => $ids ) {
		if ( count($ids) < 4 ) continue ;
		$sql = "INSERT INTO common_names_human (name,cnt,entry_ids,dates) VALUES ('" . $db->real_escape_string($name) . "',".count($ids).",'".implode(',',$ids)."','" . $db->real_escape_string($k) . "')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	}
}

?>