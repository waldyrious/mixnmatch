var tt ;
var wd ;
var catalogs ;
var cat_order ;
var catalog = 1 ; // TESTING
var meta ;
var api = 'api.php' ;
var widar_api = '/widar/index.php' ;
var use_bs3 = true ;
var is_testing ;
var entry_cache = {} ;
var sort_mode ;

function my_encodeURIC ( s ) {
	return escattr(encodeURIComponent(s)) ;
}

function resetMeta () {
	meta = { offset:0 , show_noq:1 , show_autoq:1 , show_userq:1 , show_na:0 , show_nowd:0 , per_page:50 } ;
}

function getUserLink ( u ) {
	if ( typeof u == 'undefined' ) return '??' ;
	if ( u.id == 0 || u.id == 3 || u.id == 4 ) return "<i>" + u.tusc_username + "</i>" ;
	return "<a class='wiki' target='_blank' href='//www.wikidata.org/wiki/User:"+escape(u.tusc_username)+"'>" + u.tusc_username + "</a>" ;
}

function getQlink ( q ) {
	if ( q*1 == -1 ) {
		return "<span style='color:red'>Not&nbsp;on&nbsp;Wikidata</span>" ;
	} else {
		var q2 = q+'' ;
		q2 = q2.replace ( /\D/g , '' ) ;
		var ret = "<a q='Q"+q+"' class='wikidata wd_unlabeled' target='_blank' href='//www.wikidata.org/wiki/Q" + q2 + "'><span class='qname'>Q" + q2 + "</span></a>" ;
		ret += "&nbsp;<span><a tt_title='view_in_reasonator' href='http://tools.wmflabs.org/reasonator/?q="+q2+"' target='_blank'><img src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border=0 /></a></span>" ;
		if ( q != 0 ) ret += "&nbsp;<small>Q"+q+"</small>" ;
		return ret ;
	}
}

var sort_tags = {} ;

function sortCatalogList () {
	if ( typeof sort_mode == 'undefined' ) sort_mode = '' ;
	sort_tags = {} ;
	$.each ( sort_mode.replace(/ /g,'_').split(',') , function ( k , v ) { sort_tags[v] = 1 } ) ;
	
	if ( sort_tags.no_complete ) { // Remove complete catalogs
		var nc = {} ;
		$.each ( catalogs , function ( k , v ) {
			if ( isCatalogComplete ( k ) ) return ;
			nc[k] = v ;
		} ) ;
		catalogs = nc ;
	}

	cat_order = [] ;
	$.each ( catalogs , function ( k , v ) {
		catalogs[k].group_name = '' ;
		cat_order.push ( k ) ;
	} ) ;

	function sortByName ( arr ) {
		return arr.sort ( function ( a , b ) {
			if ( sort_tags.by_easiest ) {
				return (catalogs[a].noq*2+catalogs[a].autoq*1) - (catalogs[b].noq*2+catalogs[b].autoq*1) ;
			} else {
				if ( catalogs[a].name.toLowerCase() > catalogs[b].name.toLowerCase() ) return 1 ;
				if ( catalogs[a].name.toLowerCase() < catalogs[b].name.toLowerCase() ) return -1 ;
				return 0 ;
			}
		} ) ;
	}
	
	if ( sort_tags.groups ) {
	
		var complete = tt.t('complete') ;
		var groups = {} ;
		$.each ( catalogs , function ( k , v ) {
			var g = v.type ;
			g = g.substr(0,1).toUpperCase() + g.substr(1).toLowerCase() ;
			if ( isCatalogComplete ( k ) && !(sort_tags.complete_inline) ) g = complete ;
			catalogs[k].group_name = g ;
			if ( typeof groups[g] == 'undefined' ) groups[g] = [] ;
			groups[g].push ( k ) ;
		} ) ;

		var group_list = [] ;
		$.each ( groups , function ( k , v ) { group_list.push ( k ) } ) ;
		group_list = group_list.sort ( function ( a , b ) { return groups[b].length - groups[a].length } ) ;

		cat_order = [] ;
		$.each ( group_list , function ( dummy , group ) {
			if ( group == complete ) return ; // Later
			var list = sortByName ( groups[group] ) ;
			$.each ( list , function ( k , v ) { cat_order.push ( v ) } ) ;
		} ) ;
		
		if ( typeof groups[complete] != 'undefined' ) {
			var list = sortByName ( groups[complete] ) ;
			$.each ( list , function ( k , v ) { cat_order.push ( v ) } ) ;
		}
		return ;
	}


	// Default
	cat_order = sortByName ( cat_order ) ;
}

function initCatalogs ( callback ) {
	startLoad() ;
	$.getJSON ( api , {
		query : 'catalogs'
	} , function ( d ) {
		catalogs = d.data ;
		sortCatalogList() ;
		$('#loading').hide() ;
		callback() ;
	} ) ;
}

function prettyPerc ( num ) {
	var n = Math.ceil(num * 10) / 10 ;
	if ( n == parseInt(n) ) n += '.0' ;
	return "&nbsp;(" + n + "%)" ;
}

function isCatalogComplete ( catalog_id ) {
	return catalogs[catalog_id].noq + catalogs[catalog_id].autoq == 0
}

function showCatalogs () {
	resetMeta() ;
	setPermalink ( 'mode=catalogs' ) ;
	
	function showPerc ( num , total ) {
		if ( typeof num == 'undefined' || num == 0 || total == 0 ) {
			return "0 (0.0%)" ;
		}
		return prettyNumber ( num ) + prettyPerc ( 100*num/total ) ;
	}

	var h = "<div style='margin-top:5px;' class='well'><span id='welcome'></span>" ;
	h += "<span tt='top_message'></span> <span tt='glam_message'></span>" ;
	h += "<div id='widar' style='display:none;color:red' tt='widar_note'></div>" ;
	h += "</div>" ;


	h += "<div class='pull-right'>" ;
	h += "Legend: " ;
	h += "<span class='label label-success' tt='manually_matched'></span>" ;
	h += "<span class='label label-info' tt='auto_matched'></span>" ;
	h += "<span class='label label-warning' tt='not_on_wikidata'></span>" ;
	h += "<span class='label label-danger' tt='n_a'></span>" ;
	h += "<span class='label label-default' tt='unmatched'></span>" ;
	h += "</div>" ;

	var table_begin = "<table class='table table-condensed table-striped'><thead><tr><th tt='catalog'></th><th colspan=2 tt='status'></th></thead><tbody>" ;
	var table_end = "</tbody></table>" ;
	var first_output = true ;

	var groupsize = {} ;
	$.each ( catalogs , function ( k , v ) {
		var g = v.group_name ;
		if ( typeof groupsize[g] == 'undefined' ) groupsize[g] = 0 ;
		groupsize[g]++ ;
	} ) ;

	var last_group_count = 0 ;
	var last_group = '' ;
	$.each ( cat_order , function ( dummy , k2 ) {
		v2 = catalogs[k2] ;
		
		if ( v2.group_name != last_group ) {
			last_group = v2.group_name ;
			if ( !first_output ) {
				h += table_end ;
//				if ( last_group_count > 0 ) h += "<div>" + last_group_count + " entries</div>" ;
			}
			last_group_count = 0 ;
			h += "<h2>" + last_group + " <small>[" + groupsize[last_group] + " " + tt.t('catalogs').toLowerCase() + "]</small></h2>" ;
			h += table_begin ;
			first_output = false ;
		}
		
		if ( first_output ) {
			h += table_begin ;
			first_output = false ;
		}
		last_group_count++ ;
		
//		console.log ( v2 ) ;
		var p_manual = 100 * v2.manual / v2.total ;
		var p_nowd = 100 * v2.nowd / v2.total ;
		var p_autoq = 100 * v2.autoq / v2.total ;
		var p_na = 100 * v2.na / v2.total ;
		var p_noq = 100 * v2.noq / v2.total ;
		var complete = false ;
		var desc = v2.desc ;
		if ( desc.length > 60 ) desc = desc.substr(0,60)+"..." ;

		h += "<tr class='catalog_row'>" ;
		h += "<td class='catalog_row_left'>" ;
		h += "<div class='catalog_left" ;
		if ( v2.active == 2 ) h += " catalog_broken" ;
		if ( v2.wd_prop == null || ( v2.wd_prop != null && v2.wd_qual != null ) ) h += " catalog_no_prop' tt_title='no_wd_prop" ;
		h += "'>" ;

//			h += "<a href='#' class='expand_catalog' catalog=" + v2.id + ">[+]</a>&nbsp;" ;
		h += "<div style='display:inline-block;width:150px'><a href='#' onclick='showCatalogDetails("+v2.id+");return false'>" + v2.name + "</a></div>" ;
		h += "<a class='external' target='_blank' title='" + (v2.desc==desc?'':v2.desc.replace(/'/g,'&quot;')) + "' href='" + v2.url + "'>" + desc + "</a>" ;

		h += "<div style='display:none' id='expandable_a_"+v2.id+"' class='sub_show'>" ;
		h += "<span tt='show'></span> <a href='?mode=catalog&catalog="+v2.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=1&show_na=0' tt='manual'></a> | " ;
		h += "<a href='?mode=catalog&catalog="+v2.id+"&offset=0&show_noq=0&show_autoq=1&show_userq=0&show_na=0' tt='auto'></a> | " ;
		h += "<a href='?mode=catalog&catalog="+v2.id+"&offset=0&show_noq=1&show_autoq=0&show_userq=0&show_na=0' tt='unmatched'></a> | " ;
		h += "<a href='?mode=catalog&catalog="+v2.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=0&show_na=0&show_nowd=1' tt='no_wikidata'></a> | " ;
		h += "<a href='?mode=catalog&catalog="+v2.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=0&show_na=1' tt='n_a'></a> | " ;
		h += showPerc(v2.noq,v2.total) + " <span tt='entries_to_do'></span>" ;
		h += "</div>" ;

		h += "</div>" ;
		h += "</td>" ;
		h += "<td>" ;
		
		h += "<div class='progress' style='width:500px'>" ;
		h += "<div class='progress-bar progress-bar-success' style='width: "+p_manual+"%' tt_title='manually_matched_p' tt1='"+showPerc(v2.manual,v2.total)+"'><span class='sr-only'></span></div>" ;
		h += "<div class='progress-bar progress-bar-info' style='width: "+p_autoq+"%' tt_title='auto_matched_p' tt1='"+showPerc(v2.autoq,v2.total)+"'><span class='sr-only'></span></div>" ;
		h += "<div class='progress-bar progress-bar-warning' style='width: "+p_nowd+"%' tt_title='not_wd_p' tt1='"+showPerc(v2.nowd,v2.total)+"'><span class='sr-only'></span></div>" ;
		h += "<div class='progress-bar progress-bar-danger' style='width: "+p_na+"%' tt_title='n_a_p' tt1='"+showPerc(v2.na,v2.total)+"'><span class='sr-only'></span></div>" ;
//			h += "<div class='progress-bar progress-bar-default' style='width: "+p_noq+"%' title='Unmatched: "+showPerc(v2.noq,v2.total)+"'><span class='sr-only'></span></div>" ;
		h += "</div>" ;

		h += "<div style='display:none' id='expandable_b_"+v2.id+"' class='sub_show'>" ;
		if ( isCatalogComplete ( k2 ) ) { // + v2.nowd
			h += "<b tt='complete'></b> " ;
			complete = true ;
		} else {
			h += "<a href='?mode=random&catalog="+v2.id+"&submode=unmatched' tt='game_mode'></a> | " ;
		}
		h += "<a href='?mode=create&catalog="+v2.id+"' tt='create_missing'></a> | " ;
		h += "<a href='#' tt_title='site_stats_for' tt1='"+v2.name+"' onclick='showSitestats("+v2.id+");return false' tt='site_stats'></a> | " ;
		h += "<a tt_title='download_for' tt1='"+v2.name+"' href='"+api+"?query=download&catalog="+v2.id+"' tt='download'></a> | " ;
		h += "<a tt_title='show_disambig_for' tt1='"+v2.name+"' href='?mode=disambig&catalog="+v2.id+"' tt='disambig'></a>" ;
		if ( v2.wd_prop != null ) h += " | <a href='#' tt_title='sync_with_wd' tt1='"+v2.name+"' onclick='showSync("+v2.id+");return false' tt='sync'></a>" ;
		h += "</div>" ;
		
		h += "</td>" ;
		
		h += "<td>" ;
		if ( complete ) h += "<img tt_title='all_matched' src='//upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Yes_check.svg/20px-Yes_check.svg.png' />" ;
		h += "</td>" ;
		
		h += "</tr>" ;
	} ) ;
	h += table_end ;
//	if ( last_group_count > 0 ) h += "<div>" + last_group_count + " entries</div>" ;

	
//	h += "<hr/>" ;
//	h += "<a href='#' onclick='initCatalogs(showCatalogs);return false' tt='update_list'></a>" ;
	$('#results').html(h).show() ;
	tt.updateInterface ( $('#results') ) ;
	
	$('div.progress').css({'margin-bottom':'0px'}) ;
/*
	$('a.expand_catalog').click ( function () {
		var catalog = $(this).attr('catalog') ;
		$('#expandable_a_'+catalog).toggle() ;
		$('#expandable_b_'+catalog).toggle() ;
		return false ;
	} ) ;
*/	

	$('div.sub_show a').addClass('text-info') ;

	$('tr.catalog_row').hover ( function () {
		$(this).find('div.sub_show').show() ;
	} , function () {
		$(this).find('div.sub_show').hide() ;
	} ) ;
	
}

function prettyDate ( ts ) {
	if ( ts == null ) return "????-??-?? ??:??:??" ;
	return ts.substr(0,4)+'-'+ts.substr(4,2)+'-'+ts.substr(6,2)+'&nbsp;'+ts.substr(8,2)+':'+ts.substr(10,2)+':'+ts.substr(12,2) ;
}

function filterDesc ( desc ) {
	var o = $.parseHTML("<span>"+desc.replace(/</g,'&lt;').replace(/>/g,'&gt;')+"</span>") ;
	return $(o).html() ;
}

function getRow1 ( v , user , options ) {
	if ( options === undefined ) options = {} ;
	var h = '' ;
	var state = 'noq' ;
	if ( v.q != null ) state = v.user == 0 ? 'autoq' : 'userq' ;
	h += "<td nowrap><span class='"+state+"'>" ;
	if ( options.showCatalogPrefix ) {
		h += "<a href='#' onclick='showCatalogDetails("+v.catalog+");return false'>" + catalogs[v.catalog].name + "</a>:" ;
	}
	var the_name = v.ext_name ;
	if ( the_name == '' ) the_name = '<i>#' + v.ext_id + "</i>" ;
	if ( v.ext_url == '' ) {
		h += "<span tt_title='no_url' tt1='"+v.ext_id+"'>"+the_name+"</a></span>" ;
	} else {
		h += "<a class='external' target='_blank' href='" + v.ext_url + "' tt_title='external_id' tt1='"+v.ext_id+"'>"+the_name+"</a>" ;
	}
	h += "</span>" ;
	h += "</td>" ;
	h += "<td style='width:100%'>" + filterDesc(v.ext_desc) + "</td>" ;

	h += "<td nowrap>" ;
	if ( v.q == null ) {
		h += "<div><i tt='not_matched'></i></div>" ;
	} else {
		if ( v.user == 0 ) {
			h += "<div><i tt='auto_matched'></i></div>" ;
		} else {
			h += "<div tt_title='matched_on' tt1='"+prettyDate(v.timestamp)+"' class='user_match'><span tt='matched_by'></span>" + getUserLink ( user ) + "</div>" ;
		}
	}
	h += "</td>" ;
	return h ;
}

function canCreateNewWikidataItem ( entry_id ) {
	var entry = entry_cache[entry_id] ;
	if ( typeof entry == 'undefined' ) return false ;
	var cat = catalogs[entry.catalog] ;
	if ( typeof cat.wd_prop != 'undefined' && cat.wd_qual == null && !entry.ext_id.match(/^fake_id_/) ) return true ;
	return false ;
}

function getRow2 ( v , options ) {
	if ( options === undefined ) options = {} ;
	var h = '' ;
	entry_cache[v.id] = v ;
	if ( v.q == null ) {
		var search = v.ext_name ;
		search = search.replace ( /&lt;.+?&gt;/g , '' ) ;
		var search_wd = search ;
		var search_wp = search ;
//		if ( catalogs[catalog].type == 'biography' ) {
		if ( v.type == 'person' ) {
			search_wp = search_wp.replace ( /\b[A-Z]\./g , '' ) ;
			search_wp = search_wp.replace ( /\b[A-Z][a-z]\./g , '' ) ;
			var m = v.ext_desc.match(/\d{4}/g) ;
			if ( m != null ) {
				$.each ( m , function ( kk , vv ) {
					if ( vv*1 > 2050 ) return ; // Not a year, probably...
					search_wp = $.trim ( search_wp + ' ' + vv ) ;
				} ) ;
			}
		}
		var lang = typeof catalogs[catalog] == 'undefined' ? 'en' : catalogs[catalog].search_wp ;
		h += "<td colspan='2' style='padding-left:20px'>" ;
		h += "<a class='wikidata' target='_blank' href='//www.wikidata.org/w/index.php?search="+my_encodeURIC(search_wd)+"&button=&title=Special%3ASearch' tt='search_wd'></a> | " ;
		h += "<a class='wiki' target='_blank' href='//"+lang+".wikipedia.org/w/index.php?search="+my_encodeURIC(search_wp)+"&title=Special%3ASearch' tt='search_wikipedia' tt1='"+lang+"'></a> | " ;
		h += "<a class='external' target='_blank' href='https://www.google.com/#q="+my_encodeURIC(search_wp)+"+site%3Awikipedia.org' tt='google_wikipedia'></a> | " ;
		h += "<a class='external' target='_blank' href='https://www.google.com/#q="+my_encodeURIC(search_wp)+"+site%3Awikidata.org' tt='google_wikidata'></a> | " ;
		h += "<a class='external' target='_blank' tt_title='creation_warning' href='https://www.wikidata.org/w/index.php?title=Special:NewItem&label="+my_encodeURIC(v.ext_name)+"&description="+my_encodeURIC(v.ext_desc)+"' tt='create_item'></a>" ;
		h += "</td>" ;
		h += "<td nowrap>" ;
		if ( tusc.logged_in ) {
			var label2 = canCreateNewWikidataItem ( v.id ) ? 'new_item' : 'no_wd' ;
			var title2 = canCreateNewWikidataItem ( v.id ) ? 'create_on_wd' : 'mark_as_no_wd' ;
			h += "<a href='#' tt_title='enter_q' class='green' onclick='matchEntryQ(" + v.id + ");return false' tt='set_q'></a>" ;
			h += " | <a href='#' tt_title='" + title2 + "' class='red' onclick='matchEntryQ(" + v.id + ",-1);return false' tt='" + label2 + "'></a>" ;
			h += " | <a href='#' tt_title='mark_as_na' class='yellow' onclick='matchEntryQ(" + v.id + ",0);return false' tt='n_a'></a>" ;
		} else {
			h += "<i tt='log_into_widar'></i>" ;
		}
		h += "</td>" ;
	} else {
		h += "<td style='padding-left:20px'>" ;
		h += getQlink ( v.q ) ;
		h += "</td>" ;
		h += "<td>" ;
		h += "<span class='qdesc' q='Q"+v.q+"'></span>" ;
		h += "<span class='qdescmanual' q='Q"+v.q+"'></span>" ;
		h += "</td>" ;
		h += "<td nowrap>" ;
		if ( v.user == 0 ) {
			if ( tusc.logged_in ) {
				h += "<a href='#' tt_title='confirm_auto' class='green' onclick='matchEntryQ(" + v.id + ","+v.q+");return false' tt='confirm'></a> | " ;
				h += "<a href='#' tt_title='remove_auto' class='red' onclick='removeEntryQ(" + v.id + ");return false' tt='remove'></a>" ;
				h += " | <a href='#' tt_title='mark_as_na' class='yellow' onclick='matchEntryQ(" + v.id + ",0);return false' tt='n_a'></a>" ;
			} else {
				h += "<i tt='log_into_widar'></i>" ;
			}
		} else {
			if ( tusc.logged_in ) {
				h += "<div><a href='#' tt_title='remove_match' class='red' onclick='removeEntryQ(" + v.id + ");return false' tt='remove'></a></div>" ;
			}
		}
		h += "</td>" ;
	}
	return h ;
}

function updateWD (stop) {
	var ql = [] ;
	$('a.wd_unlabeled').each ( function () {
		var o = $(this) ;
		var q = o.attr('q') ;
		if ( q == 'Q0' ) {
			o.removeClass ( 'wd_unlabeled' ) ;
			o.attr ( { tt:'n_a' } ) ; // o.text ( "N/A" ) ;
			tt.updateInterface ( o ) ;
			$('span.qdesc[q="'+q+'"]').html ( "<i tt='not_relevant'></i>" ) ;
			tt.updateInterface ( $('span.qdesc[q="'+q+'"]') ) ;
		} else if ( undefined === wd.items[q] ) {
			ql.push ( q ) ;
		} else {
			o.removeClass ( 'wd_unlabeled' ) ;
			o.text ( wd.items[q].getLabel() ) ;
			$('span.qdesc[q="'+q+'"]').text ( "..." ) ;
			var man_desc = wd.items[q].getDesc() ;
//			if ( man_desc != '' ) $('span.qdesc[q="'+q+'"]').after ( "<span class='qdesc' style='color:#777'>; " + man_desc + "</span>" ) ;
			if ( man_desc != '' ) $('span.qdescmanual[q="'+q+'"]').text ( "; "+man_desc ) ;
			
			$.get ( '/autodesc/?q='+q+'format=json' , function ( d ) {
				var t = [] ;
				if ( (d.result||'') != '' ) t.push ( d.result ) ;
//				if ( (d.manual_description||'') != '' ) t.push ( d.manual_description ) ;
				$('span.qdesc[q="'+q+'"]').text ( t.join('; ') ) ;
			} , 'json' ) ;
			
		}
	} ) ;
	
	tt.updateInterface() ;
	
	if ( ql.length == 0 ) return ; // My work here is done!
	if ( stop ) return ; // Avoid recursive loop; paranoia
	
	$('#loading').show() ;
	wd.getItemBatch ( ql , function () {
		$('#loading').hide() ;
		updateWD(1) ;
	} ) ;
}


function getTableFromData ( d , options ) {
	var h = '' ;
	
	var col = 'col2' ;
	var cnt = 0 ;
	$.each ( d.data.entries , function ( k , v ) {
		col = col=='col2'?'col1':'col2' ;
		var user = d.data.users[v.user] ;
		h += "<tr catalog_number='"+v.catalog+"' class='"+col+"' rowtype='1' entry='"+v.id+"'>" ;
		h += getRow1 ( v , user , options ) ;
		h += "</tr>" ;

		h += "<tr catalog_number='"+v.catalog+"' class='"+col+"' rowtype='2' entry='"+v.id+"'>" ;
		h += getRow2 ( v , options ) ;
		h += "</tr>" ;
		cnt++ ;
		if ( options.max_entries !== undefined && cnt >= options.max_entries ) return false ; // Max reached
	} ) ;

	if ( h != '' ) {
		var h2 = '' ;
		h2 += "<table class='table table-condensed'>" ;
		h2 += "<thead><tr><th tt='title_q'></th><th tt='description'></th><th tt='actions'></th></tr></thead>" ;
		h2 += "<tbody>" ;
		h2 += h ;
		h2 += "</tbody></table>" ;
		h = h2 ;
	}
	return h ;
}

function showCatalog ( id ) {
	if ( undefined !== id ) {
		catalog = id ; // Otherwise, keep current catalog
		startLoad() ;
	}
	var pl = [ 'mode=catalog' , 'catalog='+catalog ] ;
	$.each ( meta , function ( k , v ) { pl.push ( k+'='+v ) ; } ) ;
	setPermalink ( pl.join('&') ) ;
	$('#loading').show() ;
	$.getJSON ( api , {
		query : 'catalog' ,
		catalog : catalog ,
		meta : JSON.stringify ( meta )
	} , function ( d ) {
		var h = '' ;
		h += "<h1><a href='#' onclick='showCatalogDetails("+catalog+");return false'>" + catalogs[catalog].name + "</a></h1>" ;
		h += "<div><a class='external' target='_blank' href='" + catalogs[catalog].url + "'>" + catalogs[catalog].desc + "</a></div>" ;

		var h2 = '' ;
		if ( meta.offset > 0 ) h2 += "<a href='#' onclick='meta.offset-=meta.per_page;showCatalog();return false'>" + (meta.offset-meta.per_page+1) + "-" + (meta.offset) + "</a> | " ;
		h2 += "<b><a href='#' onclick='showCatalog();return false'>" + (meta.offset+1) + "-" + (meta.offset+meta.per_page) + "</a></b> | " ;
		h2 += "<a href='#' onclick='meta.offset+=meta.per_page;showCatalog();return false'>" + (meta.offset+meta.per_page+1) + "-" + (meta.offset+meta.per_page*2) + "</a> | " ;
		h2 += "<label style='display:inline'><input type='checkbox' id='show_noq' value='1' "+(meta.show_noq==1?'checked':'')+"/> <span tt='show_unmatched'></span></label> | " ;
		h2 += "<label style='display:inline'><input type='checkbox' id='show_autoq' value='1' "+(meta.show_autoq==1?'checked':'')+"/> <span tt='show_automatched'></span></label> | " ;
		h2 += "<label style='display:inline'><input type='checkbox' id='show_userq' value='1' "+(meta.show_userq==1?'checked':'')+"/> <span tt='show_usermatched'></span></label> | " ;
		h2 += "<label style='display:inline'><input type='checkbox' id='show_nowd' value='1' "+(meta.show_nowd==1?'checked':'')+"/> <span tt='show_nowd'></span></label> | " ;
		h2 += "<label style='display:inline'><input type='checkbox' id='show_na' value='1' "+(meta.show_na==1?'checked':'')+"/> <span tt='show_n_a'></span></label> | " ;
		h2 += "<a href='#' onclick='showSitestats("+id+");return false' tt='site_stats'></a>" ;

		h += "<div id='catalog_meta'><a href='#' name='the_start'></a>" + h2 + "</div>" ;
		
		h += getTableFromData ( d , {} ) ;

		h += "<div>" + h2 + "<br/>&nbsp;</div>" ;
		
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		window.location.hash = 'the_start' ;
		$('#catalog_meta input').change ( function () {
			meta.show_noq = $('#show_noq').is(':checked') ? 1 : 0 ;
			meta.show_autoq = $('#show_autoq').is(':checked') ? 1 : 0 ;
			meta.show_userq = $('#show_userq').is(':checked') ? 1 : 0 ;
			meta.show_na = $('#show_na').is(':checked') ? 1 : 0 ;
			meta.show_nowd = $('#show_nowd').is(':checked') ? 1 : 0 ;
			showCatalog() ;
		} ) ;
		updateWD() ;
	} ) ;
}

function updateEntryRow ( entry , callback ) {
	var tr1 = $('tr[rowtype=1][entry='+entry+']') ;
	var tr2 = $('tr[rowtype=2][entry='+entry+']') ;
	$('#loading').show() ;
//	tr1.css({'border-right':'3px solid red'}) ;
//	tr2.css({'border-right':'3px solid red'}) ;
	$.getJSON ( api , {
		query : 'get_entry' ,
		entry : entry
	} , function ( d ) {
		var v ;
		$.each ( d.data.entries , function ( a , b ) { v = b ; } ) ;
		var user = d.data.users[v.user] ;
		$('#loading').hide() ;
		if ( typeof callback != 'undefined' ) {
			callback() ;
			return ;
		}
		tr1.html ( getRow1 ( v , user ) ) ;
		tr2.html ( getRow2 ( v ) ) ;
		updateWD() ;
	} ) ;
}

function getWidar ( params , callback ) {
	params.tool_hashtag = "mix'n'match" ;
	$.get ( widar_api , params , function ( d ) {
		if ( d.error != 'OK' ) {
			console.log ( params ) ;
			if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
				console.log ( "ERROR (re-trying)" , params , d ) ;
				setTimeout ( function () { getWidar ( params , callback ) } , 500 ) ; // Again
			} else {
				console.log ( "ERROR (aborting)" , params , d ) ;
				var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
				$('#out ol').append(h) ;
				callback ( d ) ; // Continue anyway
			}
		} else {
			callback ( d ) ;
		}
	} , 'json' ) . fail(function() {
		console.log ( "Again" , params ) ;
		getWidar ( params , callback ) ;
	}) ;
}

var delay_to_fix_wikidata_bug = 1 ; // Workaround for rapid wikidata editing bug; may be unnecessary, but does not do harm

function setItemProp ( _q , _p , _tq , callback ) {
	var p = 'P'+(''+_p).replace(/\D/g,'') ;
	var q = 'Q'+(''+_q).replace(/\D/g,'') ;
	var tq = 'Q'+(''+_tq).replace(/\D/g,'') ;
	var params = { botmode:1 , action:'set_claims' , ids:q , prop:p , target:tq } ;
//	console.log ( 'ADDING ITEM LINK' , params ) ;
	getWidar ( params , function (d) { setTimeout(function(){callback(d)},delay_to_fix_wikidata_bug) }  ) ;
}

function setStringProp ( _q , _p , s , callback ) {
	var p = 'P'+(''+_p).replace(/\D/g,'') ;
	var q = 'Q'+(''+_q).replace(/\D/g,'') ;
	var params = { botmode:1 , action:'set_string' , id:q , prop:p , text:s } ;
//	console.log ( 'ADDING STRING' , params ) ;
	getWidar ( params , function (d) { setTimeout(function(){callback(d)},delay_to_fix_wikidata_bug) }  ) ;
}

function setItemLabel ( _q , lang , s , callback ) {
	var q = 'Q'+(''+_q).replace(/\D/g,'') ;
	var params = { botmode:1 , action:'set_label' , q:q , lang:lang , label:s } ;
//	console.log ( 'SETTING LABEL' , params ) ;
	getWidar ( params , function (d) { setTimeout(function(){callback(d)},delay_to_fix_wikidata_bug) }  ) ;
}

function setItemDescription ( _q , lang , s , callback ) {
	var q = 'Q'+(''+_q).replace(/\D/g,'') ;
	var params = { botmode:1 , action:'set_desc' , q:q , lang:lang , label:s } ;
//	console.log ( 'SETTING DESCRIPTION' , params ) ;
	getWidar ( params , function (d) { setTimeout(function(){callback(d)},delay_to_fix_wikidata_bug) }  ) ;
}

function createNewItem ( entry_id , entry_cat , callback ) {
	// TODO: Create new Wikidata item
//	console.log ( entry_id , entry_cat ) ;

	var params = { botmode:1 , action:'create_blank_item' } ;
//	console.log ( 'CREATING ITEM' , params ) ;
	getWidar ( params , function ( d0 ) {
//		console.log ( 'd0' , d0 ) ;
		var q = d0.q ;
		if ( typeof q == 'undefined' || q == '' ) {
			alert ( tt.t('problem_creating_item') ) ;
			return callback();//updateEntryRow ( entry_id , callback ) ;
		}
		
		setItemLabel ( q , entry_cat.search_wp , entry_cat.ext_name , function ( d1 ) {
//			console.log ( 'd1' , d1 ) ;
		
			setItemDescription ( q , entry_cat.search_wp , entry_cat.ext_desc , function ( d2 ) {
//				console.log ( 'd2' , d2 ) ;
				setStringProp ( q , entry_cat.wd_prop , entry_cat.ext_id , function ( d3 ) {
//					console.log ( 'd3' , d3 ) ;
					
					if ( entry_cat.entry_type == 'person' ) {
						setItemProp ( q , 'P31' , 'Q5' , function ( d4 ) {
//							console.log ( 'd4' , d4 ) ;
							callback(q);//updateEntryRow ( entry_id , callback ) ;
						} ) ;
					} else {
						callback(q);//updateEntryRow ( entry_id , callback ) ;
					}
				} ) ;
			} ) ;
			
		} ) ;
		
	} ) ;
}

function matchEntryQ ( entry , q , callback ) {
	if ( undefined === q ) {
		var reply = prompt ( tt.t('enter_q_number') , "" ) ;
		if ( reply === null ) return ;
		q = reply.replace ( /\D/g , '' ) ;
		if ( q == '' ) return ;
	}


	$.post ( api , {
		query:'match_q' ,
		tusc_user:tusc.user ,
		tusc_pass:tusc.pass ,
		tusc_project:tusc.language+'.'+tusc.project ,
		entry:entry ,
		q:q
	} , function ( d ) {
	
		if ( d.status != 'OK' ) { alert ( d.status ) ; return false }
	
		if ( canCreateNewWikidataItem ( entry ) ) { // Set directly on Wikidata
//		if ( typeof d.entry.wd_prop != 'undefined' && d.entry.wd_qual == null && !d.entry.ext_id.match(/^fake_id_/) ) { // Set directly on Wikidata
			if ( q*1 == -1 ) {
				var tr2 = $('tr[rowtype=2][entry='+entry+']') ;
				tr2.html ( "<td colspan='2'><i tt='creating_item'></i></td>" ) ;
				tt.updateInterface ( tr2 ) ;
				$('#loading').show() ;
				createNewItem ( entry , d.entry , function ( nq ) {
					$('#loading').hide() ;
					matchEntryQ ( entry , nq.replace(/\D/g,'') , callback ) ;
				} ) ;
// WAS:				updateEntryRow ( entry , callback ) ;
			} else if ( q*1 > 0 ) {
			
				setStringProp ( q , d.entry.wd_prop , d.entry.ext_id , function () {
					updateEntryRow ( entry , callback ) ;
				} ) ;
			
			} else {
				updateEntryRow ( entry , callback ) ;
			}
		} else { // Only store in mix'n'match for now
			updateEntryRow ( entry , callback ) ;
		}

	} , 'json' ) ;
}



function removeEntryQ ( entry ) {
	$.post ( api , {
		query:'remove_q' ,
		tusc_user:tusc.user ,
		tusc_pass:tusc.pass ,
		tusc_project:tusc.language+'.'+tusc.project ,
		entry:entry
	} , function ( d ) {

		if ( d.status != 'OK' ) { alert ( d.status ) ; return false }

		updateEntryRow ( entry ) ;
//		showCatalog ( catalog ) ;
	} , 'json' ) ;
}

function showSearch ( query , exclude , include ) {
	if ( typeof exclude == 'undefined' ) exclude = '' ;
	if ( typeof include == 'undefined' ) include = '' ;
	if ( undefined === query ) query = '' ;
	setPermalink ( 'mode=search&query='+query+'&exclude='+exclude+'&include='+include ) ;
	exclude = exclude=='' ? [] : exclude.split(',') ;
	include = include=='' ? [] : include.split(',') ;
	
	
	var h = "<h1 tt='search'></h1>" ;
	h += "<div><form id='search_form' class='form form-inline'>" ;
	h += "<span tt='search_query'></span> <input type='text' id='search_query' /> <input id='search_button' type='submit' class='btn btn-primary' tt_value='find' /> <span id='subcat_dropdown'></span>" ;
	h += " <a href='#' id='exclude_link' tt='exclude_catalogs'></a>" ;
	h += "<div style='display:none' id='exclude_box'><span tt='exclude_box'></span><br/>" ;
	
	$.each ( cat_order , function ( dummy , k ) {
		v = catalogs[k] ;
		var checked = $.inArray ( k , exclude ) >= 0 ? ' checked ' : '' ;
		if ( include.length > 0 ) {
			checked = $.inArray ( k , include ) >= 0 ? '' : ' checked ' ;
		}
		h += "<label style='font-weight:unset'><input type='checkbox' class='exclude_cb' name='exclude[" + k + "]' value='" + k + "' " + checked + "/> " + v.name + "</label> | " ;
	} ) ;
	h += "</div>" ;
	h += "</form></div>" ;
	h += "<div id='search_results'></div>" ;
	if ( query != '' ) h += "<hr/><div><a href='https://www.wikidata.org/w/index.php?title=Special:NewItem&label=" + escape(query) + "' target='_blank' class='external'><span tt='create_item'></span> \"" + query + "\"</a></div>" ;
	$('#results').html(h).show() ;
	$('#exclude_link').click ( function () { $('#exclude_box').toggle() ; return false } ) ;
	$('#search_button').focus() ;
	$('#search_query').val ( query ) ;
	$('#search_button').click ( function () {
		exclude = [] ;
		$('input.exclude_cb:checked').each ( function () {
			exclude.push ( $(this).val() ) ;
		} ) ;
		showSearch ( $('#search_query').val() , exclude.join(',') ) ;
		return false ;
	} ) ;
	$('#search_query').focus() ;
	if ( exclude.length > 0 ) $('#exclude_box').show() ;
	
	if ( query == '' ) return ;
	
	
//	alert ( "2" ) ;
	$("#search_results").html ( '<i tt="searching"></i>' ) ;
	tt.updateInterface ( $('#results') ) ;	
	$.post ( api , {
		query:'search' ,
		what:query,
		exclude:exclude.join(',')
	} , function ( d ) {
		var h = getTableFromData ( d , { showCatalogPrefix:true } ) ;
		if ( h == '' ) h = "<i tt='no_matches'></i>" ;
		$("#search_results").html ( h ) ;
		tt.updateInterface ( $('#results') ) ;

		var cats = {} ;
		$.each ( d.data.entries , function ( k , v ) {
			cats[v.catalog] = v.catalog ;
		} ) ;
		h = "<select id='sub_cats'>" ;
		h += "<option value='0' selected tt='all_catalogs'></option>" ;
		$.each ( cats , function ( k , v ) {
			h += "<option value='"+v+"'>"+catalogs[v].name+"</option>" ;
		} ) ;
		h += "</select>" ;
		$('#subcat_dropdown').html ( h ) ;
		$('#sub_cats').change ( function () {
			var cat = $(this).val() * 1 ;
			if ( cat == 0 ) {
				$('tr[catalog_number]').show() ;
			} else {
				$('tr[catalog_number]').hide() ;
				$('tr[catalog_number="'+cat+'"]').show() ;
			}
		} ) ;

		updateWD() ;
//		alert ( "3" ) ;
	} ) ;
}


function showRecentChanges ( ts ) {
	startLoad() ;
	if ( ts === undefined ) ts = '' ;
	setPermalink ( 'mode=rc&ts='+ts ) ;
	$.post ( api , {
		query:'rc' ,
		ts:ts
	} , function ( d ) {
		var h = '' ;
		h += "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th tt='date'></th><th tt='catalog_item'></th><th tt='action_taken'></th><th tt='user'></th></tr></thead>" ;
		h += "<tbody>" ;
		$.each ( d.data.events , function ( k , v ) {
			h += "<tr>" ;
			h += "<td>" + prettyDate ( v.timestamp ) + "</td>" ;
			h += "<td>" ;
			h += "<a href='#' onclick='showCatalogDetails("+v.catalog+");return false'>" + catalogs[v.catalog].name + "</a>:" ;
			h += "<a class='external' target='_blank' href='"+v.ext_url+"'>"+v.ext_name+"</a> <small>("+v.ext_id+")</small>" ;
			h += "</td>" ;
			h += "<td nowrap>" ;
			if ( v.event_type == 'match' ) {
				if ( v.q == 0 ) {
					h += "<span tt='marked_as_not_relevant'></span>" ;
				} else {
					if ( v.q > -1 ) h += "<span tt='matched_to'></span> " ;
					h += getQlink ( v.q ) ;
				}
			} else if ( v.event_type == 'remove_q' ) {
				h += "<span style='color:red' tt='wikidata_was_unlinked'></span>" ;
			} else {
				h += v.event_type ;
			}
			h += "</td>" ;
			h += "<td nowrap>by " + getUserLink ( d.data.users[v.user] ) + "</td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
	} , 'json' ) ;
}

function showMissingPages ( cat , site ) {
	var max = 250 ; // Max entries
	cat *= 1 ;
	startLoad() ;
	setPermalink ( 'mode=missingpages&catalog='+cat+'&site='+site ) ;
	$.post ( api , {
		query:'missingpages',
		catalog:cat,
		site:site
	} , function ( d ) {
		var noe = 0 ;
		$.each ( d.data.entries , function ( k , v ) { noe++ } ) ;
		var h = '' ;
		h += "<h1><span tt='missing_articles_on'></span> "+site+"</h1>" ;
		h += "<div><span tt='associated_1'></span> <a href='#' onclick='showCatalogDetails("+cat+");return false'>" + catalogs[cat].name + "</a><span tt='associated_2'></span> " + site + ". " ;
		if ( noe > max ) {
			h += max + " <span tt='of'></span> " ;
		}
		h += noe+" <span tt='entries_shown'></span></div>" ;
		h += getTableFromData ( d , { max_entries:max } ) ;
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		updateWD() ;
	} , 'json' ) ;
}

function asAttr ( s ) {
	return $('<div/>').html(s).text();
}

function startLoad () {
	$('#results').hide() ;
	$('#results2').hide() ;
	$('#loading').show() ;
}

function showSync ( cat ) {
	startLoad() ;
	cat *= 1 ;
	setPermalink ( 'mode=sync&catalog='+cat ) ;
	$.get ( api , {
		query:'get_sync',
		catalog:cat
	} , function ( d ) {
		if ( d.status != 'OK' ) {
			alert ( d.status ) ;
			return ;
		}
//		console.log ( d ) ;

		var update_ids = [] ;

		var bad_q = {} ;
		$.each ( d.data.mm_dupes , function ( k , v ) {
			bad_q[v[0]] = 1 ;
		} ) ;
		
		var h = '' ;
/*
		if ( d.data.mm_dupes.length > 0 ) {
			h += "<h2>Internal duplicates</h2><div class='lead'>Two or more entries with the same Q number. Might be OK, just flagging...</div>" ;
			h += "<div style='max-height:300px;overflow:auto;'>" ;
			$.each ( d.data.mm_dupes , function ( dummy , v ) {
				var q = v[0] ;
				h += "<h3><a href='//www.wikidata.org/wiki/Q" + q + "' target='_blank'>Q" + q + "</a></h3>" ;
				$.each ( v[1] , function ( k2 , v2 ) {
					h += "<div>" + v2 + "</div>" ;
				} ) ;
			} ) ;
			h += "</div>" ;
		}
*/		
		$('#results').html(h).show() ;
		
		
		h = '' ;
		h += "<div class='lead' tt='lag_note'></div>" ;

		if ( d.data.wd_no_mm.length > 0 ) {
			console.log ( d.data.wd_no_mm ) ;
			h += "<h2 tt='wikidata_connections' tt1='" + d.data.wd_no_mm.length + "'></h2><div>" ;
			h += "<div tt='different_overwrite'></div>" ;
			h += "<form id='wd_no_mm'>" ;
			h += "<input type='submit' class='btn btn-primary' tt_value=\"update_mnm\" />" ;
			h += "</form>" ;
			h += "</div>" ;
		}
		
		var warning = '' ;

		var mm_no_wd_list = '' ;
		if ( d.data.mm_no_wd.length > 0 ) {
			var max = 3000 ;
			h += "<h2 tt='connections_only_here' tt1='" + d.data.mm_no_wd.length + "'></h2><div>" ;
			h += '<form action="//tools.wmflabs.org/wikidata-todo/quick_statements.php" method="post" target="_blank">' ;
			var rows = [] ;
			$.each ( d.data.mm_no_wd , function ( k , v ) {
				var s ;

				if ( d.data.qual != '' ) {
					if ( d.data.prop == '958' ) {
						s = "Q" + v[0] + "\tP1343\tQ" + d.data.qual ;
						s += "\tP" + d.data.prop + "\t\"" + (''+v[1]).replace(/"/,'\\"') + "\"" ;
						warning = "<div class='lead' style='color:red' tt='stats_warning'></div>" ;
					} else if ( d.data.prop == '972' ) {
						s = "Q" + v[0] + "\tP528\t\"" + (''+v[1]).replace(/"/,'\\"') + "\"" ;
						s += "\tP" + d.data.prop + "\tQ" + d.data.qual ;
					} else {
						alert ( tt.t('something_wrong') ) ;
					}
				} else {
					s = "Q" + v[0] + "\tP" + d.data.prop + "\t\"" + (''+v[1]).replace(/"/,'\\"') + "\"" ;
				}
				
				rows.push ( s ) ;
				if ( rows.length >= max ) {
					h += "(<span tt='adding_only' tt1='"+max+"'></span>) " ;
					return false ;
				}
			} ) ;
			mm_no_wd_list = rows.join("\n") ;
			h += "<input type='hidden' id='mm_no_wd_list' name='list' value='' />" ;
			h += "<input type='submit' class='btn btn-primary' tt_value='update_wikidata' name='yup' /> (<span tt='qs_note'></span>)" ;
			h += "</form>" ;
			h += "</div>" ;
		}
		
		if ( d.data.qual == '' ) {
			h += "<h2 tt='double_ids'></h2>" ;
			h += "<div><a href='http://wdq.wmflabs.org/stats?action=doublestring&prop=" + d.data.prop + "' target=_blank' tt='check_double_id_usage'></a>" ;
			h += "<span tt='then_remove'></span></div>" ;
		}

		h += warning ;
		
//		if ( d.data.mm_double.length > 0 ) {
			h += "<h2 tt='double_q'></h2>" ;
			h += "<div>" ;
			h += "<ol>" ;
			$.each ( d.data.mm_double , function ( q , v ) {
				h += "<li><b>Q" + q + "</b>: " + v.join('; ') + "<table><tbody>" ;
				$.each ( v , function ( k2 , v2 ) {
					update_ids.push ( v2 ) ;
					h += "<tr rowtype='1' entry='" + v2 + "'></tr>" ;
					h += "<tr rowtype='2' entry='" + v2 + "'></tr>" ;
				} ) ;
				h += "</tbody></table></li>" ;
			} ) ;
			h += "</ol>" ;
			h += "</div>" ;
//		}

		$('#loading').hide() ;
		$('#results2').html(h).show() ;
		tt.updateInterface ( $('#results2') ) ;
		$('#mm_no_wd_list').attr ( { 'value' : mm_no_wd_list } ) ;
		
		$.each ( update_ids , function ( k , v ) {
			updateEntryRow ( v ) ;
		} ) ;

/*
function updateEntryRow ( entry ) {
	var tr1 = $('tr[rowtype=1][entry='+entry+']') ;
	var tr2 = $('tr[rowtype=2][entry='+entry+']') ;
*/
		
		$('#wd_no_mm').submit ( function ( e ) {
			e.preventDefault() ;
			$('#wd_no_mm').html ( "<span tt='updating'></span>" ) ;
			tt.updateInterface ( $('#wd_no_mm') ) ;
			$.post ( api , {
				query:'match_q_multi' ,
				catalog:cat,
				tusc_user:tusc.user ,
				tusc_pass:tusc.pass ,
				tusc_project:tusc.language+'.'+tusc.project ,
				data:JSON.stringify(d.data.wd_no_mm)
			} , function ( d ) {

				console.log ( d ) ;
				if ( d.status == 'OK' ) {
					$('#wd_no_mm').html ( "<span tt='done'></span>" ) ;
				} else {
					$('#wd_no_mm').html ( "<span tt='error'></span>" + d.status ) ;
				}
				tt.updateInterface ( $('#wd_no_mm') ) ;
//				console.log ( d.data ) ;
			} ) ;
			return false ;
		} ) ;
	} ) ;
}


function showCreate ( cat ) {
	startLoad() ;
	cat *= 1 ;
	setPermalink ( 'mode=create&catalog='+cat ) ;
	$.post ( api , {
		query:'create',
		catalog:cat
	} , function ( d ) {
		if ( d.status != 'OK' ) {
			alert ( d.status ) ;
			return ;
		}
		
		function removeHTML ( s ) {
			return $.trim ( s.replace(/<.+?>/g,'') ) ;
		}
		
		var c = catalogs[cat] ;
		console.log ( c ) ;
		var lang = c.search_wp ;
		var t = [] ;
		$.each ( d.data , function ( dummy , v ) {
			t.push ( "CREATE" ) ;
			if ( v.ext_name != '' ) {
				t.push ( "LAST\tL"+lang+"\t\""+removeHTML(v.ext_name)+"\"" ) ;
				if ( lang != 'en' ) t.push ( "LAST\tLen\t\""+removeHTML(v.ext_name)+"\"" ) ;
			}
			if ( v.ext_desc != '' && v.ext_desc != null ) t.push ( "LAST\tD"+lang+"\t\""+removeHTML(v.ext_desc)+"\"" ) ;
			if ( v.type == 'person' ) t.push ( "LAST\tP31\tQ5" ) ;
			if ( c.wd_prop != null && c.wd_qual == null ) {
				var s = "LAST\tP" + c.wd_prop + "\t\"" + v.ext_id + "\"" ;
				if ( v.ext_url != null && v.ext_url != '' ) s += "\tS854\t\"" + v.ext_url + "\"" ;
				t.push ( s ) ;
			} else if ( c.wd_prop != null && c.wd_qual != null ) {
				var s = "LAST\tP1343\tQ" + c.wd_qual + "\tP" + c.wd_prop + "\t\"" + v.ext_id + "\"" ;
				if ( v.ext_url != null && v.ext_url != '' ) s += "\tS854\t\"" + v.ext_url + "\"" ;
				t.push ( s ) ;
			}
			
		} ) ;
		
		var h = "<div>" ;
		h += "<h2 tt='creation_commands'></h2>" ;
		h += "<form method='post' action='/wikidata-todo/quick_statements.php' target='_blank'>" ;
		h += "<textarea name='list' style='width:100%' rows=20>" ;
		h += t.join("\n") ;
		h += "</textarea>" ;
//		h += '<input name="doit" value="Do it" class="btn btn-primary" type="submit">' ;
		h += "</form>" ;
		h += "</div>" ;
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
	} ) ;
}

function showSitestats ( cat ) {
	startLoad() ;
	cat = cat === undefined ? '' : cat ;
	setPermalink ( 'mode=sitestats&catalog='+cat ) ;
	$.post ( api , {
		query:'sitestats',
		catalog:cat
	} , function ( d ) {
		var h = '' ;

		var cat_ids = [] ;
		if ( cat == '' ) {
			h += "<h1 tt='site_stats_all'></h1>" ;
			h += "<div tt='articles_per_site'></div>" ;
			$.each ( catalogs , function ( id , o ) { cat_ids.push ( id ) } ) ;
		} else {
			h += "<h1><span tt='site_stats_for'></span> " + catalogs[cat].name + "</h1>" ;
			h += "<div tt='articles_per_site_catalog'></div>" ;
			cat_ids.push ( cat ) ;
		}
		
		h += "<table id='sitestat_table' class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th tt='site'></th>" ;
		$.each ( cat_ids , function ( dummy , id ) { h += "<th class='numbers'>" + catalogs[id].name + "</th>" ; } ) ;
		h += "</tr></thead>" ;
		h += "<tbody>" ;
		$.each ( d.data , function ( site , v ) {
			h += "<tr>" ;
			h += "<th>" + site + "</th>" ;
			$.each ( cat_ids , function ( dummy , id ) {
				var n = (v[id]||0)*1 ;
				var t = (d.total[id]||0)*1 ;
				h += "<td nowrap class='numbers'>" ;
				h += "<a href='#' tt_title='show_missing_pages' tt1='"+site+"' onclick='showMissingPages("+id+",\""+site+"\");return false'>" ;
				h += n + prettyPerc ( 100*n/t )  ;
				h += "</a></td>" ;
			} ) ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
		if ( cat != '' ) $('#sitestat_table').width('auto');
	} , 'json' ) ;
}

function showDisambig ( cat ) {
	var h = "<span tt='loading_50'></span>" ;
	$('#results').html(h).show() ;
	tt.updateInterface ( $('#results') ) ;
	setPermalink ( 'mode=disambig' ) ;
	$.post ( api , {
		query:'disambig',
		catalog:(cat||'')
	} , function ( d ) {
		$('#loading').hide() ;
		var h = getTableFromData ( d , { showCatalogPrefix:true } ) ;
		$('#results').html(h).show() ;
		updateWD() ;
	} ) ;
}


function showCreationCandidates () {
	var h = "<span tt='loading_set_multiple'></span>" ;
	$('#results').html(h).show() ;
	tt.updateInterface ( $('#results') ) ;
	setPermalink ( 'mode=creation_candidates' ) ;
	$.post ( api , {
		query:'creation_candidates'
	} , function ( d ) {
		$('#loading').hide() ;
		var h = getTableFromData ( d , { showCatalogPrefix:true } ) ;
		h += "<hr/>" ;
		h += "<button class='btn btn-primary' id='next_set'>Next set</button>" ;
		h += " <a target='_blank' href='//www.wikidata.org/w/index.php?title=Special:NewItem&label=" + escape(d.data.name) + "'><span tt='create_new_item_for'></span> \"" + d.data.name + "\"</a>" ;
		h += " | <a target='_blank' href='//commons.wikimedia.org/w/index.php?search=" + escape('"'+d.data.name+'"') + "&title=Special%3ASearch&go=Go' tt='search_commons'></a>" ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
		$('#next_set').click ( function () {
			showCreationCandidates() ;
		} ).focus() ;
		updateWD() ;
	} ) ;
}

function showSameNames () {
	var h = "<span tt='loading_set_multiple'></span>" ;
	$('#results').html(h).show() ;
	tt.updateInterface ( $('#results') ) ;
	setPermalink ( 'mode=same_names' ) ;
	$.post ( api , {
		query:'same_names'
	} , function ( d ) {
		$('#loading').hide() ;
		var h = getTableFromData ( d , { showCatalogPrefix:true } ) ;
		h += "<hr/>" ;
		h += "<button class='btn btn-primary' id='next_set'>Next set</button>" ;
		h += " <a target='_blank' href='//www.wikidata.org/w/index.php?title=Special:NewItem&label=" + escape(d.data.name) + "'><span tt='create_new_item_for'></span> \"" + d.data.name + "\"</a>" ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
		$('#next_set').click ( function () {
			showSameNames() ;
		} ).focus() ;
		updateWD() ;
	} ) ;
}

function showEntry ( entry_id ) {
	$('#results').html("<i tt='loading'></i>").show() ;
	tt.updateInterface ( $('#results') ) ;
	setPermalink ( 'mode=entry&entry='+entry_id ) ;
	$.post ( api , {
		query:'get_entry' ,
		entry:entry_id
	} , function ( d ) {
		$('#loading').hide() ;
		var h = getTableFromData ( d , { showCatalogPrefix:true } ) ;
		$('#results').html(h).show() ;
		updateWD() ;
	} ) ;
}

function showCatalogDetails ( cat ) {
	$('#results').html("<i tt='loading'></i>").show() ;
	tt.updateInterface ( $('#results') ) ;
	setPermalink ( 'mode=catalog_details&catalog='+cat ) ;

	function showPerc ( num , total ) {
		if ( typeof num == 'undefined' || num == 0 || total == 0 ) {
			return "0.0%" ;
		}
		var ret = prettyPerc ( 100*num/total ) + '' ;
		ret = ret.replace ( /[^0-9.%]/g , '' ) ;
		return ret ;
	}

	var c = catalogs[cat] ;
	var h = '' ;
	h += "<style>td.num { text-align:right; font-family:Courier; font-size:11pt; }</style>" ;
	h += "<h1><span tt='details_for'></span> " + c.name + "</h1>" ;
	h += "<div class='lead'><a href='" + c.url + "' target='_blank' class='external'>" + c.desc + "</a></div>" ;

	h += "<div>" ;
	h += "<a href='?mode=catalog&catalog="+c.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=1&show_na=0' tt='manual'></a> | " ;
	h += "<a href='?mode=catalog&catalog="+c.id+"&offset=0&show_noq=0&show_autoq=1&show_userq=0&show_na=0' tt='auto'></a> | " ;
	h += "<a href='?mode=catalog&catalog="+c.id+"&offset=0&show_noq=1&show_autoq=0&show_userq=0&show_na=0' tt='unmatched'></a> | " ;
	h += "<a href='?mode=catalog&catalog="+c.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=0&show_na=0&show_nowd=1' tt='no_wikidata'></a> | " ;
	h += "<a href='?mode=catalog&catalog="+c.id+"&offset=0&show_noq=0&show_autoq=0&show_userq=0&show_na=1' tt='n_a'></a> | " ;
	if ( c.noq + c.nowd == 0 ) complete = true ;
	else h += "<a href='?mode=random&catalog="+c.id+"&submode=unmatched' tt='game_mode'></a> | " ;
	h += "<a href='?mode=create&catalog="+c.id+"' tt='create_missing'>Create missing items</a> | " ;
	h += "<a href='#' title='Site statistics for "+c.name+"' onclick='showSitestats("+c.id+");return false' tt='site_stats'></a> | " ;
	h += "<a title='Download matched data for "+c.name+"' href='"+api+"?query=download&catalog="+c.id+"' tt='download'></a> | " ;
	h += "<a title='Show disambiguation items linked for "+c.name+"' href='?mode=disambig&catalog="+c.id+"' tt='disambig'></a>" ;
	if ( c.wd_prop != null ) h += " | <a href='#' title='Synchronize "+c.name+" with Wikidata' onclick='showSync("+c.id+");return false' tt='sync'></a>" ;
	h += " | <a href='?mode=search&include="+c.id+"' tt='search_this_catalog'></a>" ;
	h += " | <a title='Visual matching tool' href='./visual_match.html#catalog="+c.id+"' tt='visual_tool'></a>" ;
	if ( c.wd_prop != null && c.wd_qual == null ) {
		var wdfist = '/fist/wdfist/?depth=3&language=en&project=wikipedia&sparql=SELECT%20?item%20WHERE%20{%20?item%20wdt:P'+c.wd_prop+'%20?dummy%20}&no_images_only=1&remove_used=1&remove_multiple=1&prefilled=1' ;
		h += " | <a class='external' target='_blank' href='"+wdfist+"' tt='find_images'></a>" ;
//		h += " | <a class='external' target='_blank' href='/fist/wdfist/?remove_used=1&no_images_only=1&prefilled=1&wdq=claim["+c.wd_prop+"]'>Find images</a>" ;
	}
	h += "</div>" ;	
	
	var leftover = c.total - c.manual - c.autoq - c.nowd - c.na ;
	h += "<div><h2 tt='entries'></h2>" ;
	h += "<table class='table table-striped'><tbody>" ;
	h += "<tr><th tt='manually_matched'></th><td class='num'>" + prettyNumber(c.manual) + "</td><td class='num'>" + showPerc(c.manual,c.total) + "</td></tr>" ;
	h += "<tr><th tt='auto_matched'></th><td class='num'>" + prettyNumber(c.autoq) + "</td><td class='num'>" + showPerc(c.autoq,c.total) + "</td></tr>" ;
	h += "<tr><th tt='not_on_wikidata'></th><td class='num'>" + prettyNumber(c.nowd) + "</td><td class='num'>" + showPerc(c.nowd,c.total) + "</td></tr>" ;
	h += "<tr><th tt='not_applicable'></th><td class='num'>" + prettyNumber(c.na) + "</td><td class='num'>" + showPerc(c.na,c.total) + "</td></tr>" ;
	h += "<tr><th tt='unmatched'></th><td class='num'>" + prettyNumber(leftover) + "</td><td class='num'>" + showPerc(leftover,c.total) + "</td></tr>" ;
	h += "<tr><th tt='total'></th><td class='num'><b>" + prettyNumber(c.total) + "</b></td><td></td></tr>" ;
	h += "</tbody></table>" ;
	h += "</div>" ;
	
	$.get ( api , {
		query:'catalog_details' ,
		catalog:cat
	} , function ( d ) {
	
		h += "<div><h2>Entry types</h2>" ;
		h += "<table class='table table-striped'><tbody>" ;
		h += "<thead><tr><th></th><th style='text-align:right' tt='entries'></th></tr></thead><tbody>" ;
		$.each ( (d.data.type||[]) , function ( k , v ) {
			h += "<tr><th>" + (v.type==''?'<i tt="unknown"></i>':v.type) + "</th><td class='num'>" + prettyNumber(v.cnt) + "</td></tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		h += "</div>" ;

		h += "<div><h2 tt='matches_over_time'></h2>" ;
		h += "<table class='table table-striped table-condensed'><tbody>" ;
		h += "<thead><tr><th tt='year_month'></th><th></th><th style='text-align:right' tt='matches'></th></tr></thead><tbody>" ;
		$.each ( (d.data.ym||[]) , function ( k , v ) {
			var perc = 100 * v.cnt / ( c.total - leftover ) ;
			h += "<tr><th>" + v.ym.substr(0,4)+'-'+v.ym.substr(4,2) + "</th>" ;
			h += "<td>" ;
			h += "<div class='progress' style='width:980px'>" ;
			h += "<div class='progress-bar progress-bar-success' style='width: "+perc+"%'><span class='sr-only'></span></div>" ;
			h += "</td>" ;
			h += "<td class='num'>" + prettyNumber(v.cnt) + "</td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		h += "</div>" ;

		h += "<div><h2 tt='users'></h2>" ;
		h += "<table class='table table-striped'><tbody>" ;
		h += "<thead><tr><th tt='user'></th><th style='text-align:right' tt='matches'></th></tr></thead><tbody>" ;
		$.each ( (d.data.user||[]) , function ( k , v ) {
			if ( v.uid == 3 || v.uid == 4 || v.username.match(/^\d+\.\d+\.\d+\.\d+$/) ) {
				h += "<tr><td>" + v.username + "</td><td class='num'>" + prettyNumber(v.cnt) + "</td></tr>" ;
			} else {
				h += "<tr><td><a href='//www.wikidata.org/wiki/User:" + escape(v.username) + "' class='external' target='_blank'>" + v.username + "</a></td><td class='num'>" + prettyNumber(v.cnt) + "</td></tr>" ;
			}
		} ) ;
		h += "</tbody></table>" ;
		h += "</div>" ;
	
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
	} ) ;
}

var random_run = false ;

function showRandom ( cat , submode ) {

	function nextRandom() {
		if ( 1 ) showRandom ( cat , submode ) ;
		else window.location.reload();
	}

	startLoad() ;
	if ( !random_run ) {
		random_run = true ;
		var tmp = $('#result_container form').html() ;
		$('#result_container').html ( tmp ) ;
	}
	cat = (typeof cat == 'undefined') ? '' : cat ;
	submode = (typeof submode == 'undefined') ? '' : submode ;
	setPermalink ( 'mode=random&catalog='+cat+'&submode='+submode ) ;
	$.post ( api , {
		query:'random',
		catalog:cat,
		submode:submode
	} , function ( d ) {
//		console.log ( d ) ;
		var src = {} ;
		var h = '' ;
		h += "<div style='float:right' tt='try_mobile_game' tt1='"+cat+"'></div>" ;
		h += "<h1><a href='?mode=catalog_details&catalog="+cat+"'>"+catalogs[cat].name+"</a></h1>" ;
		h += "<table class='table table-condensed table-striped'><tbody>" ;
		h += "<tr><th tt='catalog_id'></th><td>" + d.data.ext_id + "</td></tr>" ;
		h += "<tr><th tt='catalog_name'></th><td>" ; 
		if ( d.data.ext_url == '' ) h += d.data.ext_name ;
		else h += "<a target='_blank' class='external' href='" + escattr(d.data.ext_url) + "'>" + d.data.ext_name + "</a>" ;
		h += "</td></tr>" ;
		h += "<tr><th tt='catalog_desc'></th><td>" + d.data.ext_desc + "</td></tr>" ;
		h += "</tbody></table>" ;
		
		h += "<div style='text-align:center'>" ;
		h += "<span tt='enter_q_number'></span>:" ; // or drag Wikipedia link here
		h += "<form><div style='font-size:20pt'><input type='text' size='60' style='width:250px' id='q_or_wp' tt_placeholder='qxxx' />" ;
		h += " <input type='submit' tt_value='set_q' class='btn btn-primary' />" ;
		h += " <input type='button' tt_value='no_wikidata_entry' class='btn btn-warning' id='nowp' tt_title='checked_wikis' />" ;
		h += " <input type='button' tt_value='n_a' class='btn btn-danger' id='n_a' tt_title='sure_never' />" ;
		h += " <input type='button' tt_value='skip' class='btn' id='another' />" ;
		h += "</form></div></div>" ;

/*
		var search = v.ext_name ;
		search = search.replace ( /&lt;.+?&gt;/g , '' ) ;
		var search_wd = search ;
		var search_wp = search ;
		if ( catalogs[catalog].type == 'biography' ) {
			var m = v.ext_desc.match(/\d{4}/g) ;
			if ( m != null ) search_wp = $.trim ( search_wp + ' ' + m.join(' ') ) ;
		}
		h += "<td colspan='2' style='padding-left:20px'>" ;
		h += "<a class='wikidata' target='_blank' href='//www.wikidata.org/w/index.php?search="+encodeURIComponent(search_wd)+"&button=&title=Special%3ASearch'>Search Wikidata</a> | " ;
		h += "<a class='wiki' target='_blank' href='//en.wikipedia.org/w/index.php?search="+encodeURIComponent(search_wp)+"&title=Special%3ASearch'>Search English Wikipedia</a> | " ;
		h += "<a class='external' target='_blank' href='https://www.google.com/#q="+encodeURIComponent(search_wp)+"+site%3Awikipedia.org'>Google-search Wikipedias</a> | " ;
		h += "<a class='external' target='_blank' href='https://www.google.com/#q="+encodeURIComponent(search_wp)+"+site%3Awikidata.org'>Google-search Wikidata</a>" ;
*/
		
		var search = d.data.ext_name ;
		search = search.replace ( /&lt;.+?&gt;/g , '' ) ;
		var search_wd = search ;
		var search_wp = search ;
		var m = d.data.ext_desc.match(/\d{4}/g) ;
		$.each ( (m||[]) , function ( kk , vv ) {
			if ( vv*1 > 2050 ) return ; // Not a year, probably...
			search_wp = $.trim ( search_wp + ' ' + vv ) ;
		} ) ;
/*
		if ( 0 ) {
			var url = "http://www.bing.com/search?q="+encodeURIComponent(search_wp)+"+site%3Awikipedia.org" ;
			src['bing'] = url ;
			h += "<div>Google search won't show in iframes, so ... Bing!</div>" ;
			h += "<iframe id='bing' width='940px' height='400px'></iframe>" ;
		}
*/
		var lang = catalogs[cat].search_wp ;
		h += "<div id='wpsearch' style='max-height:500px;overflow:auto'><i tt='searching_wikipedia' tt1='"+lang+"'></i></div>" ;
		h += "<div><hr/>Google-search " ;
		h += "<a target='_blank' href='https://www.google.co.uk/?#q=" + my_encodeURIC(search_wp+" site:wikipedia.org") + "' class='external' tt='wikipedias'></a> | " ;
		h += "<a target='_blank' href='https://www.google.co.uk/?#q=" + my_encodeURIC(search_wp+" site:wikidata.org") + "' class='external' tt='wikidata'></a> | " ;
		h += "<a target='_blank' href='https://www.google.co.uk/?#q=" + my_encodeURIC(search_wp+" site:wikisource.org") + "' class='external' tt='wikisource'></a> | " ;
		h += "<a target='_blank' href='https://www.wikidata.org/w/index.php?title=Special%3ASearch&fulltext=1&search=" + my_encodeURIC(search_wp.replace(/\d{3,4}/g,'')+"") + "' class='wikidata' tt='wikidata'></a>" ;
		h += "</div>" ;
		h += "<div id='other_catalogs'></div>" ;

		
		$('#loading').hide() ;
		$('#results').html(h).show() ;
		tt.updateInterface ( $('#results') ) ;
		
		$.post ( api , {
			query:'search' ,
			what:d.data.ext_name
		} , function ( d2 ) {
		

			var elements = [] ;
			$.each ( d2.data.entries , function ( k , v ) {
				if ( v.catalog == d.data.catalog ) return ; // Not from the same catalog; assuming only unique entries in catalog...
				if ( typeof v.q == undefined || v.q == null  || v.q < 1 ) return ; // No point in showing unmatched entries
				elements.push ( v ) ;
			} ) ;
			if ( elements.length == 0 ) return ; // No point in showing an empty list
			if ( elements.length > 10 ) return ; // No point in showing a long, irrelevant list
			d2.data.entries = elements ;

			var h = "<h3 tt='results_other_catalogs'></h3>" ;
			h += getTableFromData ( d2 , { showCatalogPrefix:true } ) ;
//			h = h.replace ( /\b(\d{4})\b/g , '<b>\1</b>' ) ;
			$("#other_catalogs").html ( h ) ;
			updateWD() ;

		} ) ;
		

		$.getJSON ( '//'+lang+'.wikipedia.org/w/api.php?callback=?' , {
			action:'query',
			list:'search',
			format:'json',
			srsearch:search_wp
		} , function ( d ) {
			var wp_candidates = [] ;
			var h = '' ;
			
			h += "<div><h3 tt='wikipedia_search_results' tt1='"+lang+"'></h3>" ;
			h += "<table class='table table-condensed table-striped'><tbody>" ;
			$.each ( ((d.query||{}).search||[]) , function ( k , v ) {
				if ( v.title.match(/^List of /) ) return ; // List
				if ( v.title.match(/^\d+$/) ) return ; // Year
				if ( v.title.match(/^\S+\s\d{1,2}$/) ) return ; // Month
				if ( v.title.match(/\bdisambiguation\b/) ) return ; // Disambig
				if ( v.title.match(/^High Sheriff /) ) return ; // Common false positive
				if ( v.title.match(/^Mayor of /) ) return ; // Common false positive
				if ( v.snippet.match(/\bmay refer to\b/) ) return ; // Disambig
				h += "<tr>" ;
				h += "<td><a class='external' target='_blank' href='//"+lang+".wikipedia.org/wiki/" + escattr(encodeURIComponent(v.title.replace(/ /g,'_'))) + "'>" + v.title + "</a>" ;
				h += "<td>" + v.snippet + "</td>" ;
				h += "<td nowrap id='wpq" + wp_candidates.length + "'><i tt='loading_q'></i></td>" ;
				h += "</tr>" ;
				wp_candidates.push ( { wiki:lang+'wiki' , title:v.title , id:'wpq'+wp_candidates.length } ) ;
			} ) ;
			h += "</tbody></table></div>" ;
			if ( wp_candidates.length == 0 ) h = "<div><i tt='no_results_on_wikipedia' tt1='"+lang+"'></i></div>" ;
			
			$('#wpsearch').html ( h ) ;
			$('#wpsearch span.searchmatch').css({'font-weight':'bold'});
			tt.updateInterface ( $('#wpsearch') ) ;

			$.each ( wp_candidates , function ( dummy , wpc ) {
//				console.log ( wpc ) ;
				$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , {
					action:'wbgetentities',
					sites:wpc.wiki,
					titles:wpc.title,
					format:'json',
					props:''
				} , function ( d2 ) {
//					console.log ( d2 ) ;
					q = '' ;
					$.each ( (d2.entities||{}) , function ( k , v ) { q = k ; } ) ;
					q = q.replace(/\D/g,'') ;
					if ( q*1 > 1 ) {
						$('#'+wpc.id).html ( "<a href='#' tt_title='manually_set_q'>Q" + q + "</a>" ) ;
						$('#'+wpc.id+' a').click ( function () {
							$('#q_or_wp').val($(this).text()) ;
							$('#result_container form').submit() ;
						} ) ;
					} else $('#'+wpc.id).html ( '<i tt="not_found"></i>' ) ;
					tt.updateInterface ( $('#'+wpc.id) ) ;
				} ) ;
			} ) ;


		} ) ;

		entry_cache[d.data.id] = d.data ;

		$('#nowp').click ( function () {
			matchEntryQ(d.data.id,-1,function() {
				nextRandom();
			} ) ;
		} ) ;
		
		$('#n_a').click ( function () {
			matchEntryQ(d.data.id,0,function() {
				nextRandom();
			} ) ;
		} ) ;
		
		$('#another').click ( function () {
			nextRandom();
		} ) ;
		
		$('#result_container form').submit ( function (e) {
			e.preventDefault() ;
			
			function setQ ( q ) {
				q = q.replace(/\D/g,'') ;
				if ( q == '' ) {
					alert ( tt.t('give_number') ) ;
					return ;
				}
				q *= 1 ;
				if ( q == 1 ) {
					alert ( tt.t('problem_no_number') ) ;
				} else if ( q == 0 ) {
					nextRandom();
				} else if ( tusc.logged_in ) {
					matchEntryQ(d.data.id,q,function() {
						nextRandom();
					} ) ;
				} else {
					alert ( tt.t('no_widar') ) ;
				}
			}
			
			var q = $('#q_or_wp').val(); 
			var m = q.match ( /^(.+?)\.wikipedia.org\/wiki\/(.+)$/ ) ;
			if ( m != null ) {
				$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , {
					action:'wbgetentities',
					sites:m[1]+'wiki',
					titles:m[2],
					format:'json',
					props:''
				} , function ( d2 ) {
					q = '' ;
					$.each ( (d2.entities||{}) , function ( k , v ) { q = k ; } ) ;
					if ( q == '' ) {
						alert ( tt.t('could_not_find_q',{params:[m[2],m[1]]}) ) ;
					} else setQ ( q ) ;
				} ) ;
			} else setQ ( q ) ;
			
			return false ;
		} ) ;
		
		$.each ( src , function ( id , url ) {
			$('#'+id).attr ( { src:url } ) ;
		} ) ;

	} , 'json' ) ;
}

function setPermalink ( params ) {
	$('#permalink').attr ( { href : '?'+params } ) ;
}

function checkWidar ( callback ) {
	$.get ( widar_api , {
		action:'get_rights',
		botmode:1
	} , function ( d ) {
		
		if ( d.error == 'OK' ) {
			tusc.user = ((((d.result||{}).query||{}).userinfo||{}).name||'') ;
			if ( tusc.user != '' ) tusc.logged_in = true ;
		}
		if ( tusc.logged_in ) {
			$("#welcome").html ( "<b><span tt='welcome'></span> " + tusc.user + "!</b> " ) ;
		} else {
			$('#widar').show() ;
		}
		if ( typeof callback != 'undefined' ) callback() ;
	} , 'json' ) ;
}

function init () {
	wd = new WikiData ;
	checkWidar ( function () {

		var params = getUrlVars() ;
		if ( typeof params.sort_mode != 'undefined' ) sort_mode = params.sort_mode ;

		initCatalogs( function () {
	
			if ( typeof params.catalog != 'undefined' ) params.catalog = parseInt ( params.catalog ) ;
			is_testing = typeof params.testing != 'undefined' ;
			if ( undefined === params.mode ) params.mode = 'catalogs' ;
			$.each ( meta , function ( k , v ) { if ( undefined !== params[k] ) meta[k] = params[k] ; } ) ;
			meta.offset *= 1 ;
			meta.per_page *= 1 ;

			if ( params.mode == 'catalogs' ) showCatalogs() ;
			else if ( params.mode == 'catalog' ) showCatalog(params.catalog*1) ;
			else if ( params.mode == 'rc' ) showRecentChanges(params.ts) ;
			else if ( params.mode == 'search' ) showSearch(params.query,params.exclude,params.include) ;
			else if ( params.mode == 'sitestats' ) showSitestats(params.catalog) ;
			else if ( params.mode == 'missingpages' ) showMissingPages(params.catalog,params.site) ;
			else if ( params.mode == 'sync' ) showSync(params.catalog) ;
			else if ( params.mode == 'random' ) showRandom(params.catalog,params.submode) ;
			else if ( params.mode == 'create' ) showCreate(params.catalog) ;
			else if ( params.mode == 'disambig' ) showDisambig(params.catalog||'') ;
			else if ( params.mode == 'catalog details' ) showCatalogDetails(params.catalog) ;
			else if ( params.mode == 'entry' ) showEntry(params.entry) ;
			else if ( params.mode == 'same names' ) showSameNames() ;
			else if ( params.mode == 'creation candidates' ) showCreationCandidates() ;
			checkWidar() ;
		} ) ;
	} ) ;
}

$(document).ready ( function () {
//alert ( "Mix'n'match is temporarily offline for maintenance" ) ; return ;

	tusc = { user:'' , pass:'' , logged_in:false , language:'commons' , project:'wikimedia' } ;
	resetMeta() ;
	
	var params = { toolname : "Mix'n'match" , meta : "Mix'n'match" , content : 'form.html' , run : function () {

		tt = new ToolTranslation ( {
			tool : 'mix-n-match' ,
			language : 'en' ,
			fallback : 'en' ,
			highlight_missing : true ,
			onUpdateInterface : function () {
			} ,
			callback : function () {
				$('body').show() ;
				tt.addILdropdown ( '#tooltranslate_wrapper' ) ;
				$.getScript ( '/magnustools/resources/js/wikidata.js' , function () {
					setTimeout ( init , 1 ) ;
				} ) ;
			}
		} ) ;
		
	} } ;

	params.mb = './menubar_bs3.html' ;
	loadMenuBarAndContent ( params ) ;
	
	
} ) ;
