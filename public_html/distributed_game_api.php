<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( '../opendb.inc' ) ; // $db = openMixNMatchDB() ;

function ensureUserID ( $user ) {
	global $db ;
	$sql = "INSERT IGNORE user (tusc_username) VALUES ('$user')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$user_id = -1 ;
	$sql = "SELECT id FROM user WHERE tusc_username='$user'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$user_id = $o->id ;
	}
	return $user_id ;
}

function update_overview ( $cat ) {
	global $db ;

	$sql = "INSERT IGNORE INTO overview (catalog) VALUES ($cat)" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	
	$sql = "UPDATE overview SET " ;
	$sql .= "total = (SELECT count(*) FROM entry WHERE catalog=$cat)," ;
	$sql .= "noq = (SELECT sum((case when isnull(`entry`.`q`) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
	$sql .= "autoq = (SELECT sum((case when (`entry`.`user` = 0) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
	$sql .= "na = (SELECT sum((case when (`entry`.`q` = 0) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
	$sql .= "manual = (SELECT sum((case when ((`entry`.`q` > 0) and (`entry`.`user` > 0)) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
	$sql .= "nowd = (SELECT sum((case when (`entry`.`q` = -(1)) then 1 else 0 end)) FROM entry WHERE catalog=$cat)" ;
	$sql .= " WHERE catalog=$cat" ;
	
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
}


$db = openMixNMatchDB() ;

$sql = "SET CHARACTER SET utf8" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');

header('Content-type: application/json');

$callback = $_REQUEST['callback'] ;
$out = array () ;

if ( $_REQUEST['action'] == 'desc' ) {

	$in = 'in' ;
	$title = "Mix'n'match game" ;
	if ( get_request('mode','') == 'person' ) {
		$title = "Mix'n'match people game" ;
		$in = "of a person in" ;
	}

	$out = array (
		"label" => array ( "en" => $title ) ,
		"description" => array ( "en" => "Verify that an entry $in an external catalog matches a given Wikidata item. Decisions count as mix'n'match actions!" ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Bipartite_graph_with_matching.svg/120px-Bipartite_graph_with_matching.svg.png' ,
		'options' => array (
			array ( 'name' => 'Entry type' , 'key' => 'type' , 'values' => array ( 'any' => 'Any' , 'person' => 'Person' , 'not_person' => 'Not a person' , 'location' => 'Location' ) )
		)
	) ;

} else if ( $_REQUEST['action'] == 'tiles' ) {

	// GET parameters
	$num = $_REQUEST['num'] ; // Number of games to return
	$lang = $_REQUEST['lang'] ; // The language to use, with 'en' as fallback; ignored in this game
	$type = get_request ( 'type' , '' ) ;
	
	$catalogs = array() ;
	$sql = "SELECT * FROM catalog WHERE wd_prop is not null and wd_qual is null AND `active`=1" ;
	$sql .= " AND id NOT IN (80,150)" ; // Hard-excluding some catalogs
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while ( $o = $result->fetch_object() ) $catalogs[''.$o->id] = $o ;
	$cid = implode ( ',' , array_keys($catalogs) ) ;
	
	for ( $n = 1 ; $n <= $num ; $n++ ) {
		$r = rand() / getrandmax() ;
		$sql = "select * from entry where catalog IN ($cid) and user=0 and ext_desc!='' and ext_url!='' and random>=$r" ;
		if ( $type == 'person' ) $sql .= " AND entry.type='person'" ;
		if ( $type == 'not_person' ) $sql .= " AND entry.type!='person'" ;
		if ( $type == 'location' ) $sql .= " AND entry.type='location'" ;
		$sql .= " order by random limit 1" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$o = $result->fetch_object() ;
		$o->wd_prop = $catalogs[$o->catalog]->wd_prop ;
		$o->name = $catalogs[$o->catalog]->name ;

		$q = 'Q'.$o->q ;
		$p = 'P'.$o->wd_prop ;

		$g = array(
			'id' => $o->id ,
			'sections' => array () ,
			'controls' => array ()
		) ;
		
		$g['sections'][] = array ( 'type' => 'text' , 'title' => $o->ext_name , 'url' => $o->ext_url , 'text' => $o->ext_desc."\n[from ".$o->name." catalog]" ) ;
		$g['sections'][] = array ( 'type' => 'item' , 'q' => $q ) ;
		$g['controls'][] = array (
			'type' => 'buttons' ,
			'entries' => array (
				array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => 'Yes' , 'api_action' => array ('action'=>'wbcreateclaim','entity'=>$q,'property'=>$p,'snaktype'=>'value','value'=>json_encode($o->ext_id) ) ) ,
				array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip' ) ,
				array ( 'type' => 'yellow' , 'decision' => 'n_a' , 'label' => 'N/A' , 'shortcut' => 'n' ) ,
				array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => 'No' )
			)
		) ;
		
		$out[] = $g ;
		
	}

} else if ( $_REQUEST['action'] == 'log_action' ) {

	$user = get_request('user','') ;
	$entry_id = get_request('tile',-1)*1 ;
	$decision = get_request('decision','') ;

	$ts = date ( 'YmdHis' ) ;	
	$uid = ensureUserID ( $user ) ;
	
//	$out = array ( $user , $entry_id , $decision , $ts , $uid ) ;
	
	$sql = '' ;
	if ( $decision == 'yes' ) {
		$sql = "UPDATE entry SET user=$uid,timestamp='$ts' WHERE id=$entry_id" ;
	} else if ( $decision == 'no' ) {
		$sql = "INSERT INTO log (action,entry,user,timestamp) VALUES ('remove_q',$entry_id,$uid,'$ts')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$sql = "UPDATE entry SET user=null,timestamp=null,q=null WHERE id=$entry_id" ;
	} else if ( $decision == 'n_a' ) {
		$sql = "UPDATE entry SET q=-1,user=$uid,timestamp='$ts' WHERE id=$entry_id" ;
	}
	
	if ( $sql != '' ) {
		if(!$result = $db->query($sql)) $out['error'] = 'There was an error running the query [' . $db->error . ']';
		else {
			$sql = "SELECT catalog FROM entry WHERE id=$entry_id" ;
			if(!$result = $db->query($sql)) $out['error'] = 'There was an error running the query [' . $db->error . ']';
			$o = $result->fetch_object() ;
			update_overview($o->catalog) ;
		}
	}

} else {
	$out['error'] = "No valid action!" ;
}

print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>