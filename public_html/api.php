<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 2 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
require_once ( '../opendb.inc' ) ; // $db = openMixNMatchDB() ;

//exit ( 0 ) ;

function mic_drop () {
	global $callback , $out ;
	if ( $callback != '' ) print $callback.'(' ;
	print json_encode ( $out ) ;
	if ( $callback != '' ) print ')' ;
	myflush();
	ob_end_flush() ;
}

function generateOverview ( &$out ) {
	global $db ;
	$sql = "SELECT overview.* FROM overview,catalog WHERE catalog.id=overview.catalog and catalog.active>=1" ; // overview is "manually" updated, but fast; vw_overview is automatic, but slow
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $out['data'][$o->catalog][$k] = $v ;
	}

	$sql = "SELECT * FROM catalog WHERE active>=1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
	}


	$sql = "select user.tusc_username AS username,catalog.id from catalog,user where owner=user.id AND active>=1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
	}
	
	$sql = "select catalog.id AS id,last_update from catalog,autoscrape WHERE active>=1 AND catalog.id=autoscrape.catalog and do_auto_update=1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) {
		$out['data'][$o->id]['scrape_update'] = 1 ;
		$lu = $o->last_update ;
		$lu = substr($lu,0,4).'-'.substr($lu,4,2).'-'.substr($lu,6,2).' '.substr($lu,8,2).':'.substr($lu,10,2).':'.substr($lu,12,2) ;
		$out['data'][$o->id]['last_scrape'] = $lu ;
	}
}

function addExtendedEntryData () {
	global $out , $db ;
	if ( count ( $out['data']['entries'] ) == 0 ) return ;

	$keys = implode ( ',' , array_keys ( $out['data']['entries'] ) ) ;

	// Person birth/death dates
	$sql = "SELECT * FROM person_dates WHERE entry_id IN ($keys)" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		if ( $o->born != '' ) $out['data']['entries'][$o->entry_id]->born = $o->born ;
		if ( $o->died != '' ) $out['data']['entries'][$o->entry_id]->died = $o->died ;
	}

	// Location data
	$sql = "SELECT * FROM location WHERE entry IN ($keys)" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->entry]->lat = $o->lat ;
		$out['data']['entries'][$o->entry]->lon = $o->lon ;
	}

	// Multimatch
	$sql = "SELECT * FROM multi_match WHERE entry_id IN ($keys)" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$a = array() ;
		foreach ( explode(',',$o->candidates) AS $c ) $a[] = 'Q'.$c ;
		$out['data']['entries'][$o->entry_id]->multimatch = $a ;
	}
}

function updateOverviewFile () {
	global $db ;
	$out = array ( 'status' => 'OK' , 'data' => array() ) ;

	generateOverview ( $out ) ;
	$fn = '/data/project/mix-n-match/public_html/overview.json' ;
	$s = json_encode ( $out ) ;
	file_put_contents ( $fn , $s ) ;
}

function update_overview ( $catalog ) {
	global $db , $out ;
	$catalogs = array () ;
	if ( isset ( $catalog ) ) {
		$catalogs[] = $catalog ;
	} else {
		$sql = "SELECT id from catalog" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()) $catalogs[] = $o->id ;
	}
	
	$out['catalogs'] = $catalogs ;
	foreach ( $catalogs AS $cat ) {
		$sql = "INSERT IGNORE INTO overview (catalog) VALUES ($cat)" ;
		$result = getSQL ( $db , $sql , 5 ) ;

		$sql = "SELECT count(*) AS total,sum(q is null) AS q_null,sum(q=0) AS q_zero,sum(q=-1) AS q_minus_one,sum(user=0) AS u_zero,sum(q>0 and user>0) AS manual FROM entry where catalog=$cat" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		$o = $result->fetch_object() ;
		if ( !isset($o->total) or $o->total == null or $o->total == '' or $o->total==0 ) { // No entries
			$o = (object) [ 'total'=>0,'null'=>0,'u_zero'=>0,'q_zero'=>0,'manual'=>0,'q_minus_one'=>0 ] ;
			return ;
		}

		$sql = "UPDATE overview SET " ;
		$sql .= "total = {$o->total}," ;
		$sql .= "noq = {$o->q_null}," ;
		$sql .= "autoq = {$o->u_zero}," ;
		$sql .= "na = {$o->q_zero}," ;
		$sql .= "manual = {$o->manual}," ;
		$sql .= "nowd = {$o->q_minus_one}," ;
		$sql .= "multi_match = (SELECT count(*) FROM multi_match WHERE multi_match.catalog=$cat)" ;
		$sql .= " WHERE catalog=$cat" ;
		$result = getSQL ( $db , $sql , 5 ) ;
	}
	
	updateOverviewFile() ;
}

$bad_q = array() ;
function compareSets ( $s1 , $s2 ) {
	global $bad_q ;
	$ret = array() ;
	foreach ( $s1 AS $q => $v1 ) {
		if ( isset($bad_q[$q*1]) ) continue ; // Duplicate horrors, ignore
		foreach ( $v1 AS $extid ) {
			if ( !isset ( $s2[$q] ) ) {
				$ret[] = array ( $q , utf8_encode($extid) ) ;
			} else if ( !in_array ( $extid , $s2[$q] ) ) {
				$ret[] = array ( $q , utf8_encode($extid) ) ;
			}
		}
	}
	return $ret ;
}

$userinfo = (object) array (
	"id_is_cached" => false ,
	"id" => 0 ,
	"last_block_check" => 0 ,
	"is_blocked" => false
) ;

function ensureUserID ( $user ) {
	global $db , $userinfo ;
	if ( $userinfo->id_is_cached ) return $userinfo->id ;
	$sql = "INSERT IGNORE user (tusc_username) VALUES ('$user')" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	$user_id = -1 ;
	$sql = "SELECT id,last_block_check FROM user WHERE tusc_username='$user'" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$user_id = $o->id ;
		$userinfo->last_block_check = $o->last_block_check ;
	}
	
	if ( time() - $userinfo->last_block_check > 60*15 ) { // Cache block check for 15 min
		$url = "https://www.wikidata.org/w/api.php?action=query&list=users&ususers=".urlencode($user)."&usprop=blockinfo&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$userinfo->last_block_check = time() ;
		if ( isset ( $j->query->users[0]->blockid ) ) {
			$userinfo->is_blocked = true ;
			$userinfo->last_block_check = 0 ; // Force new check next time
		}
		$sql = "UPDATE `user` SET last_block_check={$userinfo->last_block_check} WHERE id=$user_id" ;
		getSQL ( $db , $sql , 5 ) ;
	}
	
	$userinfo->id = $user_id ;
	$userinfo->id_is_cached = true ;
	return $user_id ;
}

function get_users ( $users ) {
	global $db ;
	if ( count ( $users ) == 0 ) return array() ;
	$ret = array() ;
	$sql = "SELECT * FROM user WHERE id IN (" . implode(',',array_keys($users)) . ")" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$ret[$o->id] = $o ;
	}
	return $ret ;
}

function verify_tusc2 () {
	global $tusc_user ;
	if ( $tusc_user == '' ) return false ;
	return true ;
}

$query = get_request ( 'query' , '' ) ;
$tusc_user = get_request ( 'tusc_user' , -1 ) ;
$callback = get_request ( 'callback' , '' ) ;

$db = openMixNMatchDB() ;

if ( $query == 'download' ) {
	$filename = '' ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$filename = str_replace ( ' ' , '_' , $o->name ) . ".tsv" ;
	}
	$users = array() ;
	$sql = "SELECT * FROM user" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$users[$o->id] = $o->{tusc_username} ;
	}
	header('Content-type: text/plain');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	print "Q\tID\tURL\tName\tUser\n" ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND q IS NOT NULL AND q > 0 AND user!=0" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$user = '' ;
		if(isset($o->user)){ // query ensures user != 0, but user can still be null
			$user = $users[$o->user] ;
		}
		$p = '' ;
		$p .= $o->{q} . "\t" ;
		$p .= $o->{ext_id} . "\t" ;
		$p .= $o->{ext_url} . "\t" ;
		$p .= $o->{ext_name} . "\t" ;
		$p .= $user . "\n" ;
		print $p ;
#		print utf8_encode ( $p ) ;
	}
	exit ( 0 ) ;
}



$sql = "SET CHARACTER SET utf8" ;
$result = getSQL ( $db , $sql , 5 ) ;


if ( $query == 'redirect' ) {
	$catalog = get_request ( 'catalog' , '0' ) * 1 ;
	$ext_id = get_request ( 'ext_id' , '' ) ;
	$url = '' ;
	$sql = "SELECT ext_url FROM entry WHERE catalog=$catalog AND ext_id='" . $db->real_escape_string ( $ext_id ) . "'" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $url = $o->ext_url ;
	header('Content-type: text/html');
	print '<html><head><META http-equiv="refresh" content="0;URL='.$url.'"></head><body></body></html>' ;
	exit ( 0 ) ;
}



if ( $query == 'proxy' ) header('Content-type: text/html');
else header('Content-type: application/json; charset=UTF-8');

$out = array (
	'status' => 'OK' ,
	'data' => array()
) ;

if ( $query == 'catalogs' ) {

	generateOverview ( $out ) ;

} else if ( $query == 'save_scraper' ) {

	$scraper = json_decode ( get_request ( 'scraper' , '{}' ) ) ;
	$levels = json_decode ( get_request ( 'levels' , '{}' ) ) ;
	$options = json_decode ( get_request ( 'options' , '{}' ) ) ;
	$meta = json_decode ( get_request ( 'meta' , '{}' ) ) ;
	
#	$out['scraper'] = $scraper ;
#	$out['meta'] = $meta ;

	$user = $db->real_escape_string ( $tusc_user ) ;
	$user_id = ensureUserID ( $user ) ;
	
	if ( $userinfo->is_blocked ) {
		$out['status'] = "You are blocked on Wikidata" ;
	} else {
		$prop = preg_replace ( '/\D/' , '' , $meta->property ) ;
		$cid = trim($meta->catalog_id) ;
		$exists = false ;
		if ( !preg_match ( '/^\d+$/' , $cid ) ) { // Creat new catalog
			$type = $meta->type ;
			if ( $scraper->resolve->type->use == 'Q5' ) $type = 'biography' ;
			if ( $scraper->resolve->type->use == 'person' ) $type = 'biography' ;
			$sql = "INSERT INTO catalog (`name`,`url`,`desc`,`type`," ;
			if ( $prop != '' ) $sql .= "`wd_prop`," ;
			$sql .= "`search_wp`,`owner`,`note`) VALUES (" ;
			$sql .= '"' . $db->real_escape_string($meta->name) . '",' ;
			$sql .= '"' . $db->real_escape_string($meta->url) . '",' ;
			$sql .= '"' . $db->real_escape_string($meta->desc) . '",' ;
			$sql .= '"' . $db->real_escape_string($meta->type) . '",' ;
			if ( $prop != '' ) $sql .= '"' . $prop . '",' ;
			$sql .= '"' . $db->real_escape_string($meta->lang) . '",' ;
			$sql .= $user_id . ",'Created via scraper import')" ;
			$out['sql'] = $sql ;
			$result = getSQL ( $db , $sql , 5 ) ;
			$cid = $db->insert_id ;
			$exists = true ;
		} else { // Check if catalog exists
			$sql = "SELECT * FROM catalog WHERE id=$cid" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			while($o = $result->fetch_object()) $exists = true ;
		}

		if ( !$exists ) {
			$out['status'] = "Catalog #$cid does not exist" ;
		} else { // Add autoscrape
			$out['data']['catalog'] = $cid ;
			$j = array (
				'levels' => $levels ,
				'options' => $options ,
				'scraper' => $scraper
			) ;
			$j = $db->real_escape_string(json_encode($j)) ;
			
			$existing = (object) array('bla'=>'test') ;
			$sql = "SELECT * FROM autoscrape WHERE catalog=$cid" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			while($o = $result->fetch_object()) $existing = $o ;
			
			if ( isset($existing->owner) ) {
				if ( $existing->owner == $user_id ) {
					$sql = "UPDATE autoscrape SET `json`='$j',`status`='IMPORT' WHERE catalog=$cid" ;
					$result = getSQL ( $db , $sql , 5 ) ;
				} else {
					$out['status'] = "A different user created the existing scraper" ;
				}
			} else {
				$sql = "INSERT INTO autoscrape (`catalog`,`json`,`owner`,`notes`,`status`) VALUES ($cid," ;
				$sql .= '"' . $j . '",' ;
				$sql .= "$user_id,'Created via scraper import','IMPORT')" ;
				$sql .= " ON DUPLICATE KEY UPDATE json='$j',status='IMPORT'" ;
				$result = getSQL ( $db , $sql , 5 ) ;
			}
		}

	}


} else if ( $query == 'autoscrape_test' ) {
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 

	require_once ( '/data/project/mix-n-match/autoscrape.inc' ) ;
	$json = get_request ( 'json' , '' ) ;
	$as = new AutoScrape ;
	
	if ( !$as->loadFromJSON ( $json , 0 ) ) {
		$out['status'] = $as->error ;
	} else {
		$as->max_urls_requested = 1 ;
		$as->log2array = true ;
		$as->runTest() ;
		if ( isset($as->error) AND $as->error != null ) $out['status'] = $as->error ;
		$out['data']['html'] = $as->last_html ;
		$out['data']['log'] = $as->logs ;
		$out['data']['results'] = $as->entries ;
		$out['data']['last_url'] = $as->getLastURL() ;
//print_r ( $out ) ;

		$out['data']['html'] = utf8_encode ( $out['data']['html'] ) ;
		foreach ( $out['data']['results'] AS $k => $v ) {
//			$v->name = utf8_decode($v->name) ;
//			$v->desc = utf8_decode($v->desc) ;
		}
	}

} else if ( $query == 'update_overview' ) {

	$catalog = get_request ( 'catalog' , '' ) ;
	if ( $catalog != '' ) {
		$catalogs = explode ( ',' , $catalog ) ;
		foreach ( $catalogs AS $c ) {
			$c = trim($c) * 1 ;
			if ( $c == 0 ) continue ;
			update_overview ( $c ) ;
		}
	} else update_overview() ;

} else if ( $query == 'catalog' or $query == 'get_entry' ) {
	$entry = get_request ( 'entry' , -1 ) ;
	$id = get_request ( 'catalog' , -1 ) ;
	$meta = json_decode ( get_request ( 'meta' , '' ) ) ;
	
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	
	$q = array() ;
	

	$sql = "SELECT * FROM entry WHERE " ;
	if ( $query == 'get_entry' ) {
		$ext_ids = get_request ( 'ext_ids' , '' ) ;
		if ( $ext_ids != '' ) {
			$ext_ids = json_decode ( $ext_ids ) ;
			$x = array() ;
			foreach ( $ext_ids AS $eid ) $x[] = $db->real_escape_string($eid) ;
			$ext_ids = '"' . implode ( '","' , $x ) . '"' ;
			$sql .= "catalog=" . $db->real_escape_string($id) . " AND ext_id IN ($ext_ids)" ;
		} else $sql .= "id IN (" . $db->real_escape_string($entry) . ")" ;
//		$sql .= " ORDER BY entry.id" ;
	}else {
		$sql .= "catalog=" . $db->real_escape_string($id) ;
		if ( $meta->show_multiple == 1 ) {
			$sql .= " AND EXISTS ( SELECT * FROM multi_match WHERE entry_id=entry.id ) AND ( user<=0 OR user is null )" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_nowd == 0 and $meta->show_na == 1 ) {
			$sql .= " AND q=0" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_na == 0 and $meta->show_nowd == 1 ) {
			$sql .= " AND q=-1" ;
		} else {
			if ( $meta->show_noq != 1 ) $sql .= " AND q IS NOT NULL" ;
			if ( $meta->show_autoq != 1 ) $sql .= " AND ( q is null OR user!=0 )" ;
			if ( $meta->show_userq != 1 ) $sql .= " AND ( user<=0 OR user is null )" ;
			if ( $meta->show_na != 1 ) $sql .= " AND ( q!=0 or q is null )" ;
//			if ( $meta->show_nowd != 1 ) $sql .= " AND ( q=-1 )" ;
		}
//		$sql .= " ORDER BY entry.id" ;
		$sql .= " LIMIT " . $db->real_escape_string($meta->per_page) ;
		$sql .= " OFFSET " . $db->real_escape_string($meta->offset) ;
	}
	
	$out['sql'][] = $sql ;
if ( isset($_REQUEST['testing']) ) { print_r($out) ; exit(0) ; }
	
	$users = array() ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->id] = $o ;
		if ( $o->q != null ) $q[] = $o->q ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}

	addExtendedEntryData() ;
	
	$out['data']['users'] = get_users ( $users ) ;

} else if ( $query == 'catalog_details' ) {

	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	
	$sql = "select type,count(*) as cnt from entry where catalog=$catalog group by type order by cnt desc" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $out['data']['type'][] = $o ;
	
	$sql = "select substring(timestamp,1,6) AS ym,count(*) as cnt from entry where catalog=$catalog and timestamp is not null and user!=0 group by ym order by ym" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $out['data']['ym'][] = $o ;
	
	$sql = "select tusc_username as username,entry.user AS uid,count(*) as cnt from entry,user where catalog=$catalog and entry.user=user.id and user!=0 and entry.user is not null group by uid order by cnt desc" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $out['data']['user'][] = $o ;

} else if ( $query == 'match_q' ) {
	$entry = $db->real_escape_string ( get_request ( 'entry' , -1 ) * 1 ) ;
	$q = '' . ( get_request ( 'q' , -1 ) * 1 ) ;

	if ( verify_tusc2() ) {
		$user = $db->real_escape_string ( $tusc_user ) ;
		$user_id = ensureUserID ( $user ) ;
		
		if ( $userinfo->is_blocked ) {
			$out['status'] = "You are blocked on Wikidata!" ;
		} else {
		
			$ts = date ( 'YmdHis' ) ;
			$sql = "UPDATE entry SET q=$q,user=$user_id,timestamp='$ts' WHERE id=$entry" ;
			$result = getSQL ( $db , $sql , 5 ) ;

			$sql = "DELETE FROM potential_mismatch WHERE entry_id=$entry AND q!=$q" ;
			$result = getSQL ( $db , $sql , 5 ) ;
		
			$cat = '' ;
			$sql = "SELECT *,entry.type AS entry_type FROM entry,catalog WHERE entry.id=$entry and entry.catalog=catalog.id" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			while($o = $result->fetch_object()){
				$out['entry'] = $o ;
				$cat = $o->catalog ;
			}
			
			$sql = "DELETE FROM multi_match WHERE entry_id=$entry" ;
			$result = getSQL ( $db , $sql , 5 ) ;

			mic_drop() ;
			update_overview ( $cat ) ;
			exit(0);
		}
		
	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}

} else if ( $query == 'match_q_multi' ) {

	$data = json_decode ( get_request('data','[]') ) ;
	$catalog = get_request ( 'catalog' , -1 ) * 1 ;
	
	if ( verify_tusc2() ) {
		
		$user = $db->real_escape_string ( $tusc_user ) ;
		$user_id = ensureUserID ( $user ) ;

		if ( $userinfo->is_blocked ) {
			$out['status'] = "You are blocked on Wikidata!" ;
		} else {

			$ts = date ( 'YmdHis' ) ;
		
			foreach ( $data AS $d ) {
				$q = $d[0] * 1 ;
				$extid = $db->real_escape_string ( $d[1] ) ;
				if ( $q == 0 ) continue ; // Paranoia
				$sql = "UPDATE entry SET q=$q,user=$user_id,timestamp='$ts' WHERE catalog=$catalog AND (user is null or user=0 or q=-1) AND ext_id='$extid'" ;
				$result = getSQL ( $db , $sql , 5 ) ;
			}

			update_overview ( $catalog ) ;
		}
		
	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}



} else if ( $query == 'remove_q' ) {

	$entry = $db->real_escape_string ( get_request ( 'entry' , -1 ) * 1 ) ;
	
	if ( verify_tusc2() ) {
		$user = $db->real_escape_string ( $tusc_user ) ;
		$user_id = ensureUserID ( $user ) ;

		if ( $userinfo->is_blocked ) {
			$out['status'] = "You are blocked on Wikidata!" ;
		} else {

			$ts = date ( 'YmdHis' ) ;
			$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE id=$entry" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			$sql = "INSERT INTO log (action,entry,user,timestamp) VALUES ('remove_q',$entry,$user_id,'$ts')" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			$sql = "DELETE FROM potential_mismatch WHERE entry_id=$entry" ;
			$result = getSQL ( $db , $sql , 5 ) ;

			$sql = "SELECT * FROM entry WHERE id=$entry" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			while($o = $result->fetch_object()){
				update_overview ( $o->catalog ) ;
			}
		}
		
	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}

} else if ( $query == 'rc' ) {

	$limit = 100 ;
	$ts = get_request ( 'ts' , '' ) ;
	$events = array() ;
	
	$sql = "SELECT * FROM entry WHERE user not in (0,3,4) and timestamp IS NOT NULL" ;
	if ( $ts != '' ) $sql .= " AND timestamp >= '" . $db->real_escape_string($ts) . "'" ;
	$sql .= " ORDER BY timestamp DESC LIMIT $limit" ;
	$min_ts = '' ;
	$max_ts = $ts ;
	$result = getSQL ( $db , $sql , 5 ) ;
	$users = array() ;
	while($o = $result->fetch_object()){
		$o->event_type = 'match' ;
		$events[$o->timestamp] = $o ;
		if ( $min_ts == '' ) $min_ts = $o->timestamp ;
		$max_ts = $o->timestamp ;
		$users[$o->user] = 1 ;
	}

	$sql = "SELECT entry.id AS id,catalog,ext_id,ext_url,ext_name,ext_desc,action AS event_type,log.user AS user,log.timestamp AS timestamp FROM log,entry WHERE log.entry=entry.id AND log.timestamp BETWEEN '$max_ts' AND '$min_ts'" ;
	$out['sql'] = $sql ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$events[$o->timestamp] = $o ;
		$users[$o->user] = 1 ;
	}
	
	krsort ( $events ) ;
	
	while ( count ( $events ) > $limit ) array_pop ( $events ) ;
	
	$out['data']['events'] = $events ;
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'search' ) {
	
	$max_results = get_request('max',100) * 1 ;
	$what = get_request ( 'what' , '' ) ;
	$exclude = preg_replace ( '/[^0-9,]/' , '' , get_request ( 'exclude' , '' ) ) ;
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	
	if ( $exclude == '' ) $exclude = array() ;
	else $exclude = explode ( ',' , $exclude ) ;
	$sql = "SELECT id FROM catalog WHERE `active`!=1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $exclude[] = $o->id ;
	$exclude = implode ( ',' , $exclude ) ;
	
	$s = array() ;
	$what = preg_replace ( '/[\-]/' , ' ' , $what ) ;
	$what2 = explode ( " " , $what ) ;
	foreach ( $what2 AS $w ) {
		$w = $db->real_escape_string ( trim ( $w ) ) ;
		if ( $w == '' ) continue ;
		if ( strlen($w)<3 or strlen($w)>84 ) continue ;
		$s[] = $w ;
//		$s[] = "(MATCH(ext_name) AGAINST('".$w."'))" ; // Use fulltext index
//		$s[] = "(ext_name LIKE \"%" . $w . "%\")" ; // Use slow LIKE
	}
	
//	$sql = "SELECT * FROM entry WHERE ext_name LIKE '%" . $db->real_escape_string ( $what ) . "%' LIMIT 25" ;
//	$sql = "SELECT * FROM entry WHERE " . implode ( " AND " , $s ) ;
	$sql = "SELECT * FROM entry WHERE MATCH(ext_name) AGAINST('+".implode(",+",$s)."' IN BOOLEAN MODE)" ;
	if ( $exclude != '' ) $sql .= " AND catalog not in ($exclude)" ;
	$sql .= " LIMIT $max_results" ;
	
	if ( preg_match ( '/^\s*[Qq]{0,1}(\d+)\s*$/' , $what , $m ) ) {
		$sql = "SELECT * FROM entry WHERE q=".$m[1] ;
	}
	
	$out['sql'] = $sql ;
	$db->set_charset('utf8mb4'); 
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}


	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'create' ) {
	
	$out['total'] = array() ;
	$catalog = get_request ( 'catalog' , '' ) ;
	if ( 1 ) { // Blank
		$sql = "select ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE q=-1 AND user>0 AND catalog=" . $db->real_escape_string ( $catalog ) . " ORDER BY ext_name" ;
	} else { // non-assessed, careful!
		$sql = "select ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE q IS null AND catalog=" . $db->real_escape_string ( $catalog ) . " ORDER BY ext_name" ;
	}
	$out['sql'] = $sql ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['data'][] = $o ;
	}

} else if ( $query == 'sitestats' ) {
	
	$out['total'] = array() ;
	$catalog = get_request ( 'catalog' , '' ) ;
	
	$sql = "SELECT distinct catalog,q FROM entry WHERE user>0 and q is not null" ;
	if ( $catalog != '' ) $sql .= " AND catalog=" . $db->real_escape_string ( $catalog ) ;
	$catalogs = array() ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$catalogs[$o->catalog][] = $o->q ;
	}
	
	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $qs ) {
		$qs = implode ( ',' , $qs ) ;
		$sql = "select ips_site_id,count(*) AS cnt from wb_items_per_site where ips_item_id IN ($qs) group by ips_site_id" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$out['data'][$o->ips_site_id][$cat] = $o->cnt ;
		}
	}

	$sql = "select catalog,count(*) AS cnt from entry where q>0 and user>0 group by catalog" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['total'][$o->catalog] = $o->cnt ;
	}

} else if ( $query == 'missingpages' ) {

	$catalog = $db->real_escape_string ( get_request ( 'catalog' , 0 ) ) ;
	
	$sql = "SELECT DISTINCT q FROM entry WHERE q>0 AND user>0 AND catalog=$catalog AND q is not null" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $qs[''.$o->q] = $o->q ;

	if ( count($qs) > 0 ) {
		$dbwd = openDB ( 'wikidata' ) ;
		$site = $dbwd->real_escape_string ( get_request ( 'site' , '' ) ) ;
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_item_id IN (" . implode(',',$qs) . ") AND ips_site_id='$site'" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()) {
			unset ( $qs[''.$o->ips_item_id] ) ;
		}
	}

	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$users = array() ;
	if ( count($qs) > 0 ) {
		$sql = "SELECT * FROM entry WHERE user>0 AND catalog=$catalog AND q IN (" . implode(',',$qs) . ")" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$out['data']['entries'][$o->id] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
		}
		$out['data']['users'] = get_users ( $users ) ;
	}


} else if ( $query == 'get_sync' ) {

	$catalog = $db->real_escape_string ( get_request ( 'catalog' , 0 ) * 1 ) ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	$result = getSQL ( $db , $sql , 2 ) ;
//	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$prop = $o->wd_prop ;
		$qual = $o->wd_qual ;
	}
	
	if ( !isset($prop) ) $prop = '' ;
	if ( !isset($qual) ) $qual = '' ;
	
	if ( $prop == '' ) {
		$out['status'] = 'No Wikidata property defined for this catalog' ;
		if ( $callback != '' ) print $callback.'(' ;
		print json_encode ( $out ) ;
		if ( $callback != '' ) print ')' ;
		exit ( 0 ) ;
	}
	
	$out['data']['prop'] = $prop ;
	$out['data']['qual'] = $qual ;
	
	// Get Wikidata state
	$dont_compare_values = false ;
	$query = '' ;
	$the_prop = '' ;
	if ( $qual == '' ) {
		$query = "SELECT ?q ?prop { ?q wdt:P$prop ?prop }" ;
		$the_prop = $prop ;
	} else {
		$query = "SELECT ?q ?prop { ?q p:P1343 ?statement . ?statement ps:P1343 wd:Q$qual . ?statement pq:P$prop ?prop }" ;
		$the_prop = 1343 ;
		$dont_compare_values = true ;
	}
	$out['test'] = array ( 'prop'=>$prop , 'query'=>$query ) ;
	
	$wd = array() ;
#print "$query\n" ; exit(0);
	$j = getSPARQL ( $query ) ;
	foreach ( $j->results->bindings AS $b ) {
		if ( $b->q->type != 'uri' ) continue ;
		if ( $b->prop->type != 'literal' ) continue ;
		if ( !preg_match ( '/entity\/Q(\d+)$/' , $b->q->value , $m ) ) continue ;
		$q = $m[1] * 1 ;
//		if ( $dont_compare_values ) $wd[$q][] = '' ;
//		else 
		$wd[$q][] = $b->prop->value ;
	}
	unset ( $j ) ;


	// Get Mix-n-match state
	$mm = array() ;
	$sql = "select ext_id,q from entry where q is not null and q>0 and user!=0 and user is not null and catalog=$catalog and ext_id not like 'fake_id_%'" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		if ( $o->q*1 == 0 ) continue ; // Paranoia
		$mm[$o->q*1][] = $o->ext_id ;
	}
	
	if ( $dont_compare_values ) {
		foreach ( $wd AS $q => $a ) {
			if ( isset($mm[$q]) ) $wd[$q] = $mm[$q] ;
			else unset ( $mm[$q] ) ;
		}
	}

	// Report
	$out['data']['mm_dupes'] = array() ;
	foreach ( $mm AS $q => $v1 ) {
		if ( count($v1) == 1 ) continue ;
		$b = array() ;
		foreach ( $v1 AS $v2 ) {
			$b[] = utf8_encode($v2);
		}
		$out['data']['mm_dupes'][] = array ( $q*1 , $b ) ;
		$bad_q[$q*1] = 1 ;
	}

	$out['data']['different'] = array() ;
	foreach ( $wd AS $q => $v1 ) {
		if ( !isset ( $mm[$q] ) ) continue ;
		if ( count($v1) == 1 and count($mm[$q]) == 1 and ( $dont_compare_values or $v1[0] == $mm[$q][0] ) ) continue ; // Same value
		sort($v1) ;
		sort($mm[$q]) ;
		if ( implode('|',$v1) == implode('|',$mm[$q]) ) continue ;
		$out['data']['different'][] = array ( $q , utf8_encode($v1) , utf8_encode($mm[$q]) ) ;
	}
#	print json_encode  ( $out ) ; exit ( 0 ) ;

	$out['data']['wd_no_mm'] = compareSets ( $wd , $mm ) ;
	$out['data']['mm_no_wd'] = compareSets ( $mm , $wd ) ;

	$out['data']['mm_double'] = array() ;
	$sql = "select q,count(*) as cnt,group_concat(id) AS ids from entry where q>0 and catalog=$catalog and q is not null and user is not null and user>0 group by q having cnt>1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$out['data']['mm_double'][$o->q] = explode ( ',' , $o->ids ) ;
	}
	

} else if ( $query == 'get_sync_all' ) {

	$catalogs = array() ;
	$sql = "SELECT * FROM catalog WHERE wd_prop IS NOT NULL and wd_qual IS NULL" ;
#$sql .= " AND wd_prop=1415" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()){
		$catalogs[$o->id] = $o->wd_prop ;
	}
	
	$human_issue = array() ;
	$deleted = array() ;

	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $prop ) {
		$query = 'claim[' . $prop . ']' ;
		$url = "$wdq_internal_url?q=" . urlencode ( $query ) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		
		$list = array() ;
		$qlist = array() ;
		$sql = "SELECT * FROM entry WHERE catalog=$cat AND q is not null and q>0 and user!=0" ;
		if ( count($j->items) > 0 ) $sql .= " AND q NOT IN (" . implode ( ',' , $j->items ) . ")" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$list[] = $o ;
			$qlist[] = $o->q ;
		}

		if ( count ( $list ) == 0 ) continue ;
		

		$tmp = array() ;
		$sql = "select epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ")" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$tmp[''.$o->epp_entity_id] = 1 ;
		}

		$potential_people = array() ;		
		$sql = "select distinct epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ") and not exists (select * from pagelinks where pl_from=epp_page_id and pl_namespace=0 and pl_title='Q5')" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()) $potential_people[] = $o->epp_entity_id ;

		if ( count($potential_people) > 0 ) {
			$sql = "SELECT * from entry WHERE `type`='person' and q in (" . implode(',',$potential_people) . ")" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			while($o = $result->fetch_object()) $human_issue[''.$o->q] = $o->q ;
		}
		
		foreach ( $qlist AS $q ) {
			if ( !isset ( $tmp[$q] ) ) $deleted[''.$q] = $q ;
		}

		foreach ( $list AS $o ) {
			if ( isset($deleted[$o->q]) ) continue ;
			if ( isset($human_issue[$o->q]) ) continue ;
			print "Q" . $o->q . "\tP$prop\t\"" . $o->ext_id . "\"\n" ;
		}
	}

	foreach ( $human_issue AS $q ) {
		print "HUMAN ISSUE: Q$q\n" ;
	}

	$updated = false ;
	foreach ( $deleted AS $q ) {
		if ( isset ( $_REQUEST['delete'] ) ) {
			$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE q=$q" ;
			$result = getSQL ( $db , $sql , 5 ) ;
			$updated = true ;
		} else {
			print "DELETED: Q$q\n" ;
		}
	}

	if ( $updated ) update_overview ( $o->catalog ) ;
	
	exit ( 0 ) ;
	
} else if ( $query == 'get_entries_by_q_or_value' ) {
	$q = get_request ( 'q' , '' ) ;
	$json = json_decode ( get_request ( 'json' , '{}' ) ) ;
	
	$out['data']['entries'] = [] ;
	$out['data']['catalogs'] = [] ;
	

	$sql_parts = [] ;
	$sql_parts[] = "q=" . preg_replace('/\D/','',$q) ;

	if ( count($json) > 0 ) {
		$props = implode ( ',' , array_keys((array)$json) ) ;
		$props = preg_replace ( '/[^0-9,]/' , '' , $props ) ;
		$sql = "SELECT id,wd_prop FROM catalog WHERE wd_qual IS NULL AND active=1 AND wd_prop IN ($props)" ;
#		$out['sql_catalogs_prop'] = $sql ;
		$prop2catalog = [] ;
		$result = getSQL ( $db , $sql , 2 ) ;
		while ( $o = $result->fetch_object() ) $prop2catalog['P'.$o->wd_prop][] = $o->id ;
		
		
		foreach ( $json AS $prop => $values ) {
			if ( count($values) == 0 ) continue ;
			if ( !isset($prop2catalog[$prop]) or count($prop2catalog[$prop]) == 0 ) continue ;
			foreach ( $values AS $k => $v ) $values[$k] = $db->real_escape_string ( $v ) ;
			$sql_parts[] = "catalog IN (".implode(',',$prop2catalog[$prop]).") AND ext_id IN ('".implode("','",$values)."')" ;
		}
	}
	
	$sql = "SELECT * FROM entry WHERE (" . implode ( ') OR (' , $sql_parts ) . ') ORDER BY catalog,id' ;
	$result = getSQL ( $db , $sql , 2 ) ;
	$catalog = [] ;
	while ( $o = $result->fetch_object() ) {
		$catalogs[$o->catalog] = 1 ;
		$out['data']['entries'][$o->id] = $o ;
	}
#	$out['sql'] = $sql ;
	
	if ( count($catalogs) > 0 ) {
		$sql = "SELECT * FROM catalog WHERE id IN (" . implode(',',array_keys($catalogs)) . ")" ;
		$result = getSQL ( $db , $sql , 2 ) ;
		while ( $o = $result->fetch_object() ) $out['data']['catalogs'][$o->id] = $o ;
#		$out['sql_catalogs'] = $sql ;
	}

} else if ( $query == 'get_wd_props' ) {

	$props = [] ;
	$sql = "SELECT DISTINCT wd_prop FROM catalog WHERE wd_prop IS NOT NULL AND wd_qual IS NULL AND active=1 ORDER BY wd_prop" ;
	$result = getSQL ( $db , $sql , 2 ) ;
	while ( $o = $result->fetch_object() ) $props[] = $o->wd_prop*1 ;
	print json_encode($props) ;
	exit(0);
	
} else if ( $query == 'random' ) {

	$catalogs = [] ;
	$sql = "SELECT id FROM catalog WHERE active=1" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) $catalogs[$o->id] = $o->id ;

	$submode = get_request ( 'submode' , '' ) ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$type = $db->real_escape_string ( get_request ( 'type' , '' ) ) ;
	$cnt = 0 ;
	$fail = 0 ;
	while ( 1 ) {
		if ( $fail ) break ; // No more results
		$r = rand() / getrandmax() ;
		if ( $cnt > 10 ) {
			$r = 0 ;
			$fail = 1 ;
		}
		$sql = "SELECT * FROM entry" ;
		$sql .= " FORCE INDEX (random_2)" ;
		$sql .= " WHERE random>=$r " ;
		if ( $submode == 'prematched' ) $sql .= " AND user=0" ;
		else if ( $submode == 'no_manual' ) $sql .= " AND ( user=0 or q is null )" ;
		else $sql .= " AND q IS NULL" ; // Default: unmatched
		if ( $catalog > 0 ) $sql .= " AND catalog=$catalog" ;
		if ( $type == 'Q5' or $type == 'person' ) $sql .= " AND `type` IN ('Q5','person')" ;
		else if ( $type != '' ) $sql = " AND `type`='$type'" ;
		$sql .= " ORDER BY random limit 1" ;
if ( isset($_REQUEST['id']) ) $sql = "SELECT * FROM entry WHERE id=" . ($_REQUEST['id']*1) ; // FOR TESTING
		$out['sql'] = $sql ;
		if ( isset($_REQUEST['testing']) ) break ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()) {
			if ( !isset($catalogs[$o->catalog]) ) continue ; // Make sure catalog is active
			$out['data'] = $o ;
		}
		if ( count ( $out['data'] ) > 0 ) break ;
		$cnt++ ;
	}

	if ( count($out['data']) > 0 ) {
		$id = $out['data']->id ;
		$sql = "SELECT * FROM person_dates WHERE entry_id=$id" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			if ( $o->born != '' ) $out['data']->born = $o->born ;
			if ( $o->died != '' ) $out['data']->died = $o->died ;
		}
	}

} else if ( $query == 'same_names' ) {

	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$users = array() ;
	$sql = "select ext_name,count(*) as cnt,SUM(if(q is not null or q=0, 1, 0)) AS matched from entry " ;
	if ( rand(0,10) > 5 ) $sql .= " where ext_name>'M' " ; // Hack to get more results
	$sql .= " group by ext_name having cnt>1 and cnt<10 and matched>0 and matched<cnt limit 10000" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	$tmp = array() ;
	while($o = $result->fetch_object()) {
		$tmp[] = $o ;
	}
	
	$ext_name = $tmp[array_rand($tmp)]->ext_name ;
	$out['data']['name'] = $ext_name ;
	
	$sql = "SELECT * FROM entry WHERE ext_name='" . $db->real_escape_string($ext_name) . "'" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'creation_candidates' ) {

	while ( 1 ) {
		$out['data'] = [] ;
		$min = get_request ( 'min' , 0 ) * 1 ;
		$mode = trim ( get_request ( 'mode' , '' ) ) ;
		$out['data']['entries'] = array() ;
		$out['data']['users'] = array() ;
		$users = array() ;
		
		$table = 'common_names' ;
		if ( $mode != '' ) $table .= '_' .  $db->real_escape_string ( $mode ) ;
		$sql = "select name AS ext_name,cnt FROM $table WHERE" ;
		if ( $min > 0 ) $sql .= " cnt>=$min AND" ;
		$sql .= " cnt<15 order by rand() limit 1" ;

		if ( $mode == 'test' ) $sql = "select 'thomas wright' AS ext_name,20 as cnt" ;

		$result = getSQL ( $db , $sql , 5 ) ;
		$tmp = array() ;
		while($o = $result->fetch_object()) {
			$tmp[] = $o ;
		}
		
		$ext_name = $tmp[array_rand($tmp)]->ext_name ;
		$out['data']['name'] = $ext_name ;

		$names = [ $ext_name ] ;
		if ( preg_match ( '/^(\S+) (.+) (\S+)$/' , $ext_name , $m ) ) {
			$names[] = $m[1].'-'.$m[2].' '.$m[3] ;
			$names[] = $m[1].' '.$m[2].'-'.$m[3] ;
		}
		foreach ( $names AS $k => $v ) $names[$k] = $db->real_escape_string ( $v ) ;
		
		$sql = "SELECT * FROM entry WHERE catalog IN (SELECT id FROM catalog WHERE catalog.active=1)" ;
		$sql .= " AND ext_name IN ('" . implode("','",$names) . "')" ;
		$sql .= " AND (q is null OR q!=-1)" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()) {
			$out['data']['entries'][$o->id] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
		}
		$out['data']['users'] = get_users ( $users ) ;
		addExtendedEntryData() ;
		
		$out['data']['entries'] = array_values ( $out['data']['entries'] ) ;
		if ( $min == 0 or count($out['data']['entries']) >= $min ) break ;
	}

} else if ( $query == 'disambig' ) {

	$catalog = get_request ( 'catalog' , 0 ) ;
	if ( $catalog != '' ) $catalog *= 1 ;
	
	$qs = '' ;
	$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL and q>0 and user!=0" ;
	if ( $catalog != '' ) $sql .= " AND catalog=$catalog" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) {
		if ( $qs != '' ) $qs .= "," ;
		$qs .= $o->q ;
	}
	$out['data']['qs'] = count($qs) ;
	
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$sql = "SELECT DISTINCT epp_entity_id FROM wb_entity_per_page,pagelinks WHERE epp_entity_type='item' and epp_entity_id IN ($qs) and pl_from=epp_page_id AND pl_namespace=0 AND pl_title='Q4167410' ORDER BY rand() LIMIT 50" ;

	$qs = array() ;
	$dbwd = openDB ( 'wikidata' ) ;
	$result = getSQL ( $dbwd , $sql , 5 ) ;
	while($o = $result->fetch_object()) $qs[] = $o->epp_entity_id ;
	$dbwd->close() ;
	
	$users = array() ;
	$sql = "SELECT * FROM entry WHERE q IN (" . implode(',',$qs) . ") and user!=0" ;
	$result = getSQL ( $db , $sql , 5 ) ;
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'locations' ) {

	header('Content-type: text/plain');
	
	$bbox = get_request ( 'bbox' , '' ) ;
	$bbox = preg_replace ( '/[^0-9,\.\-]/' , '' , $bbox ) ;
	$bbox = explode ( ',' , $bbox ) ;
	$out['bbox'] = $bbox ;
	
	if ( count($bbox) == 4 ) {
		$sql = "SELECT * FROM entry,location WHERE location.entry=entry.id" ;
		$sql .= " AND lon>={$bbox[0]}" ;
		$sql .= " AND lon<={$bbox[2]}" ;
		$sql .= " AND lat>={$bbox[1]}" ;
		$sql .= " AND lat<={$bbox[3]}" ;
		$sql .= " LIMIT 5000" ;
		$out['sql'] = $sql ;
		$out['data'] = array() ;
		$result = getSQL ( $db , $sql , 5 ) ;
		while($o = $result->fetch_object()) {
			$out['data'][] = $o ;
		}
	}

} else if ( $query == 'suggest' ) {

	header('Content-type: text/plain');
	$ts = date ( 'YmdHis' ) ;
	$catalog = get_request('catalog',0) * 1 ;
	$overwrite = get_request('overwrite',0) * 1 ;
	$suggestions = get_request ( 'suggestions' , '' ) ;
	$suggestions = explode ( "\n" , $suggestions ) ;
	$cnt = 0 ;
	foreach ( $suggestions AS $s ) {
		if ( trim($s) == '' ) continue ;
		$s = explode ( '|' , $s ) ;
		if ( count($s) != 2 ) {
			print "Bad row : " . implode('|',$s) . "\n" ;
			continue ;
		}
		$extid = trim ( $s[0] ) ;
		$q = preg_replace ( '/\D/' , '' , $s[1] ) ;
		$sql = "UPDATE entry SET q=$q,user=0,`timestamp`='$ts' WHERE catalog=$catalog AND ext_id='" . $db->real_escape_string($extid) . "'" ;
		if ( $overwrite == 1 ) $sql .= " AND (user=0 OR q IS NULL)" ;
		else $sql .= " AND (q IS NULL)" ;
		$result = getSQL ( $db , $sql , 5 ) ;
		$cnt += $db->affected_rows ;
	}
	print "$cnt entries changed" ;
	
	exit(0) ;

} else if ( $query == 'proxy' ) {
/*
	$url = get_request ( 'url' , '' ) ;
	$h = file_get_contents ( $url ) ;
	$h = str_replace ( '</head>' , '<base href="'.$url.'"></head>' , $h ) ;
	$h = str_replace ( 'http:' , 'https:' , $h ) ;
	print $h ;
	exit ( 0 ) ;
*/
} else {
	$out['status'] = 'Unknown action "' . $query . '"' ;
}

mic_drop () ;


?>
