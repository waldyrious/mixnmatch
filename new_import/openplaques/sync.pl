#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

# ATTENTION: This does not generate descriptions, as these are only in the individual pages!


open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":16,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'a' .. 'z' ) {
	my $url = "http://openplaques.org/people/a-z/$letter.xml" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $out = get_blank() ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m|<person uri="(http://openplaques.org/people/)(\d+)">| ) {
			$out->{id} = $2 ;
			$out->{url} = "$1$2" ;
		} elsif ( $r =~ m|<name>(.+?)</name>| ) {
			$out->{name} = $1 ;
			$out->{name} =~ s/\s*\[.+?\]\s*/ /g ;
		} elsif ( $r =~ m|</person>| ) {
			if ( defined $out->{id} ) {
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$out = get_blank() ;
		}
		
		
#		print "$id\t$url\t$name\t\n" ;
	}
}

sub get_blank {
	return { aux => [] , desc => '' , type => '' } ;
}