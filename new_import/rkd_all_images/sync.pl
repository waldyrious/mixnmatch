#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 275 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

continuous_parse ( 'https://api.rkd.nl/api/instant/images?query=&fieldset=brief&start=$1&rows=50' , 0 , 50 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'Q3305213' } ; # Painting
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########


sub parse_page {
	my ( $page , $url ) = @_ ;
	
	my $j = decode_json ( $page ) ;
	return 0 unless defined $j->{response} ;
	return 0 unless defined $j->{response}->{docs} ;
	
	my $cnt = 0 ;
	
	foreach my $i ( @{$j->{response}->{docs}} ) {
		my $out = get_blank() ;
		
		$out->{id} = $i->{priref} ;
		$out->{url} = "https://rkd.nl/en/explore/images/" . $out->{id} ;
		
		if ( defined $i->{title_engels} ) {
			$out->{name} = $i->{title_engels} ;
		} else {
			$out->{name} = $i->{benaming_kunstwerk}->[0] ;
		}
		
		my @d ;
		if ( defined $i->{toeschrijving} ) {
			my $desc = $i->{toeschrijving}->[0] ;
			if ( defined $desc->{naam_engels} ) {
				push @d , $desc->{naam_engels} || '' ;
			} elsif ( defined $desc->{naam_inverted} ) {
				push @d , $desc->{naam_inverted} || '' ;
			} else {
				push @d , $desc->{naam} || '' ;
			}
			
		}
		push @d , join $i->{datering} if defined $i->{datering} ;
		
		$out->{desc} = join "; " , @d ;
		


		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}

	return $cnt ;

=cut	
#	$page =~ s|\s+| |g ;
print "$page\n" ;
	my $cnt = 0 ;

	while ( $page =~ m|<div class="span2 record"(.+?)<div class="separator"></div>|g ) {
		my $s = $1 ;
		my $out = get_blank() ;
		
		next unless $s =~ m|\bdata-id="(\d+)"| ;
		$out->{id} = $1 ;
		$out->{url} = "https://rkd.nl/en/explore/images/$1" ;
		
		next unless $s =~ m|\bdata-title="(.+?)"| ;
		$out->{name} = $1 ;

		my @desc ;
		push @desc , $1 if $s =~ m|<div class="header">(.+?)</div>|;
		push @desc , $1 if $s =~ m|<div class="content">(.+?)</div>|;
		push @desc , $1 if $s =~ m|<div class="footer">(.+?)</div>|;

		$out->{desc} = join '; ' , @desc ;
		$out->{desc} =~ s|<.+?>| |g ;
		$out->{desc} =~ s|\s+| |g ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}
exit(0);
	return $cnt ;
=cut

}

