#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;
#use open qw(:std :utf8);

my $catalog_id = 77 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

open IN , "data.tab" ;
binmode IN, ':utf8';
while ( <IN> ) {
	chomp ;
	my ( $id , $name ) = split "\t" , $_ ;
	next unless defined $name ;
	my $out = get_blank() ;
	$out->{id} = $id ;
	$out->{url} = "http://www.catalogus-professorum-halensis.de/$id.html" ;
	$out->{name} = $name ;
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
close IN ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}
