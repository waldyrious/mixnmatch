#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$db = openMixNMatchDB() ;
$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL AND q>0" ; // AND user!=0" ;
$result = getSQL ( $db , $sql , 1 , '1' ) ;
$qlist = [] ;
while($o = $result->fetch_object()) $qlist[] = "Q{$o->q}" ;

$fix = [] ;
while ( count($qlist) > 0 ) {
	$ql2 = [] ;
	while ( count($qlist) > 0 and count($ql2) < 10000 ) $ql2[] = array_pop ( $qlist ) ;
	$ql2 = '"' . implode ( '","' , $ql2 ) . '"' ;
	$sql = "SELECT page_title,(SELECT pl_title FROM pagelinks WHERE page_id=pl_from and pl_namespace=0 limit 1) AS target FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ($ql2)" ;
	$dbwd = openDB ( 'wikidata' , 'wikidata' , true ) ;
	$result = getSQL ( $dbwd , $sql , 1 , '3' ) ;
	while($o = $result->fetch_object()){
		if ( !isset($o->target) or $o->target == null or $o->target == '' ) continue ;
		$fix[$o->page_title] = $o->target ;
	}
}

$db = openMixNMatchDB() ;
foreach ( $fix AS $from => $to ) {
	$from = preg_replace ( '/\D/' , '' , "$from" ) ;
	$to = preg_replace ( '/\D/' , '' , "$to" ) ;
	$sql = "UPDATE entry SET q=$to WHERE q=$from" ;
	getSQL ( $db , $sql , 1 , '4' ) ;
}
print count($fix) . " items redirected.\n" ;

?>