#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$catalog = $argv[1] ;
#$batch = 10000 ;
#$use_rand = 1 ;

$db = openMixNMatchDB() ;

$r = rand()/getrandmax()  ;
$sql = "select entry_id,aux_p,aux_name,catalog,auxiliary.id AS aux_id from entry,auxiliary where entry.id=entry_id and (q is null or user=0)";
if ( $use_rand ) $sql .= " and random>=$r" ;
if ( isset($catalog) ) $sql .= " and catalog=$catalog" ;
if ( $use_rand ) $sql .= " order by random" ;
if ( isset($batch) ) $sql .= " limit $batch" ;


$results = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$results[] = $o ;
}

$batchsize = 30 ;
$grouped = array() ;
foreach ( $results AS $o ) {
	$p = $o->aux_p ;
	if ( !isset($grouped[$o->catalog]) ) $grouped[$o->catalog] = array () ;
	if ( !isset($grouped[$o->catalog][$p]) ) $grouped[$o->catalog][$p] = array ( array () ) ;
	$lg = count($grouped[$o->catalog][$p])-1 ;
	if ( count($grouped[$o->catalog][$p][$lg]) >= $batchsize ) $grouped[$o->catalog][$p][++$lg] = array() ;
	$grouped[$o->catalog][$p][$lg][] = $o ;
}
#print_r ( $grouped ) ; exit ( 0 ) ;


$catalogs = array() ;
$ts = date ( 'YmdHis' ) ;

foreach ( $grouped AS $catalog => $prop_groups ) {
	foreach ( $prop_groups AS $prop => $groups ) {
		foreach ( $groups AS $group ) {
			$ids = array() ;
			$aux2entry = array() ;
			foreach ( $group AS $o ) {
				if ( preg_match ( '/"/' , $o->aux_name ) ) continue ;
				$ids[] = '"' . trim($o->aux_name) . '"' ;
				$aux2entry[trim($o->aux_name)] = $o->entry_id ;
			}
#			print "$catalog\t$prop\n" ;
			$sparql = "SELECT ?q ?id WHERE { VALUES ?id { " . implode(' ',$ids) . " } ?q wdt:P$prop ?id }" ;
			$j = getSPARQL ( $sparql ) ;
			foreach ( $j->results->bindings AS $b ) {
				$id = $b->id->value ;
				$q = preg_replace ( '/^.+\/Q/' , '' , $b->q->value ) ;
//if ( $id == 'c729681b-c5c1-4509-a6d1-f916c51aefd7' ) 
#print_r ( $aux2entry ) ;
#print_r ( $b ) ;
#continue ;
				if ( !isset($aux2entry[$id]) ) {
					print "Failed lookup aux2entry for $id\n" ;
					continue ;
				}
				$sql = "UPDATE entry SET q=$q,user=4,timestamp='$ts' WHERE id={$aux2entry[$id]} AND (q is null or user=0)" ;
#				if ( !isset($catalog) ) 
				$sql .= " AND catalog!=506" ;
#				print "$sql\n" ;
				if ( !$db->ping() ) $db = openMixNMatchDB() ;
				if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
			}
		}
	}
/*
	$sparql = 'SELECT ?q WHERE { ?q wdt:P'.$o->aux_p.' "'.$o->aux_name.'" }' ;
print "$sparql\n" ;
	$items = getSPARQLitems ( $sparql ) ;
	if ( count($items) != 1 ) continue ;
	$q = $items[0] ;
	$sql = "UPDATE entry SET q=$q,user=4,timestamp='$ts' WHERE id={$o->entry_id} AND (q IS NULL OR user=0)" ;
#	print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	$sql = "UPDATE auxiliary SET in_wikidata=1 WHERE id=" . $o->aux_id ;
	print "$sql\n" ;
continue;

	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	
#	print "Found " . $items[0] . " for ID " . $o->entry_id . "\n" ;
	$catalogs[$o->catalog] = 1 ;
*/
}

foreach ( $grouped AS $catalog => $dummy ) {
	file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=$catalog" ) ; // Update stats
}

?>